package id.evaclo.vines.view.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.evaclo.vines.R;
import id.evaclo.vines.view.adapter.StoreAdapter;
import id.evaclo.vines_service.core.BaseActivity;
import id.evaclo.vines_service.model.store.StoreDetailResponse;
import id.evaclo.vines_service.model.store.StoreListResponse;
import id.evaclo.vines_service.presenter.StorePresenter;
import id.evaclo.vines_service.service.VinesSession;
import id.evaclo.vines_service.view.StoreView;

public class StoreActivity extends BaseActivity<StoreView,StorePresenter> implements StoreView,StoreAdapter.OnStoreClickedListener {

    @BindView(R.id.toolbar_store)
    Toolbar toolbarStore;
    @BindView(R.id.text_location)
    TextView textLocation;
    @BindView(R.id.image_drop_filter)
    ImageView imageDropFilter;
    @BindView(R.id.rel_filter)
    RelativeLayout relFilter;
    @BindView(R.id.recycler_allstore)
    RecyclerView recyclerAllstore;
    LinearLayoutManager linearLayoutManager;
    VinesSession vinesSession;
    List<StoreListResponse.Data> data = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store);
        ButterKnife.bind(this);
        setToolbar();
        //initrecycleview();
        //getMvpPresenter().getListStore(106.8694690,-6.2732980);
        getMvpPresenter().getListStore(0.0,0.0);
    }
    public void setToolbar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_store);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.icons_navigation_arrow_left);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }
    public void initrecycleview(StoreListResponse response){
        data = response.getData();
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerAllstore.setHasFixedSize(true);
        recyclerAllstore.setNestedScrollingEnabled(false);
        recyclerAllstore.setLayoutManager(linearLayoutManager);
        recyclerAllstore.setAdapter(new StoreAdapter(data,this,this));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return true;
    }

    @Override
    public void onStoreClicked(StoreListResponse.Data item ) {
        Intent toStore = new Intent(StoreActivity.this,HomepageStoreActivity.class);
        toStore.putExtra("intent",item);
        //toStore.putExtra("image",item.getImage());
        //toStore.putExtra("storeName",item.getName());
        startActivity(toStore);
    }

    @Override
    public void onPhoneClicked(StoreListResponse.Data item) {
        if(!item.getPhone().equalsIgnoreCase("")) {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + item.getPhone()));
            startActivity(intent);
        }else{
            //dialog
        }
    }

    @Override
    public StorePresenter createPresenter() {
        vinesSession = new VinesSession(this);
        return new StorePresenter(this,vinesSession);
    }

    @Override
    public void failure(String message) {

    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void onSuccessGetListStore(StoreListResponse response) {
        initrecycleview(response);
    }

    @Override
    public void onSuccessGetDetailStore(StoreDetailResponse response) {

    }
}
