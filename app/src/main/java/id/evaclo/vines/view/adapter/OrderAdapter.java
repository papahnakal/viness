package id.evaclo.vines.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.evaclo.vines.R;
import id.evaclo.vines.utils.TextFormatter;
import id.evaclo.vines_service.model.order.HistoryOrderResponse;
import id.evaclo.vines_service.model.order.RecentOrderResponse;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder> {
    List<HistoryOrderResponse.Data> data;
    Context context;
    OnItemClickListener listener;

    public OrderAdapter(List<HistoryOrderResponse.Data> data, Context context, OnItemClickListener listener) {
        this.data = data;
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.holder_orders, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        HistoryOrderResponse.Data item = data.get(position);
        holder.textPrice.setText("Price : "+TextFormatter.make(context).IDRFormat(item.getTotal_order()+""));
        holder.textDate.setText("Date : "+item.getDatecreate());
        holder.textOrderNumber.setText(item.getOrder_code());
        holder.textNameStore.setText(item.getName());
        holder.textStoreAddress.setText(item.getAddress());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.text_order_number)
        TextView textOrderNumber;
        @BindView(R.id.text_date)
        TextView textDate;
        @BindView(R.id.slash)
        TextView slash;
        @BindView(R.id.text_price)
        TextView textPrice;
        @BindView(R.id.text_name_store)
        TextView textNameStore;
        @BindView(R.id.text_store_address)
        TextView textStoreAddress;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnItemClickListener {
        void onItemClicked(HistoryOrderResponse.Data data);
    }
}
