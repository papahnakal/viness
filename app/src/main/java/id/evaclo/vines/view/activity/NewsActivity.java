package id.evaclo.vines.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.evaclo.vines.R;
import id.evaclo.vines.view.adapter.NewsListAdapter;
import id.evaclo.vines.view.adapter.SpecialOfferAdapter;
import id.evaclo.vines_service.core.BaseActivity;
import id.evaclo.vines_service.model.news.NewsDetailResponse;
import id.evaclo.vines_service.model.news.NewsListResponse;
import id.evaclo.vines_service.model.product.ProductListResponse;
import id.evaclo.vines_service.presenter.NewsPresenter;
import id.evaclo.vines_service.service.VinesSession;
import id.evaclo.vines_service.view.NewsView;

public class NewsActivity extends BaseActivity<NewsView,NewsPresenter>implements NewsView,NewsListAdapter.OnItemClickListener {

    @BindView(R.id.text_toolbar_title)
    TextView textToolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recycler_news)
    RecyclerView recyclerNews;
    VinesSession vinesSession;
    List<NewsListResponse.Data> data = new ArrayList<>();
    LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        ButterKnife.bind(this);
        setToolbar();
        getMvpPresenter().getListNews();
    }

    @Override
    public NewsPresenter createPresenter() {
        vinesSession = new VinesSession(this);
        return new NewsPresenter(this,vinesSession);
    }

    public void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        textToolbarTitle.setText("NEWS");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.icons_navigation_arrow_left);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return true;
    }

    public void initRecycleProduct(NewsListResponse response) {
        //data.clear();
        data = response.getData();
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerNews.setHasFixedSize(true);
        recyclerNews.setLayoutManager(linearLayoutManager);
        recyclerNews.setAdapter(new NewsListAdapter(this,data, this));
    }

    @Override
    public void failure(String message) {

    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void onSuccessGetListNews(NewsListResponse response) {
        initRecycleProduct(response);
    }

    @Override
    public void onSuccessGetDetailtNews(NewsDetailResponse response) {

    }

    @Override
    public void onItemClicked(NewsListResponse.Data item) {
        Intent toDetail = new Intent(NewsActivity.this,DetailNewsActivity.class);
        toDetail.putExtra("id_news",item.getNews_id());
        startActivity(toDetail);
    }
}
