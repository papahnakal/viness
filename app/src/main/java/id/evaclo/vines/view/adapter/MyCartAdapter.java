package id.evaclo.vines.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.evaclo.vines.R;
import id.evaclo.vines.utils.TextFormatter;
import id.evaclo.vines_service.model.product.ProductDetailResponse;

public class MyCartAdapter extends RecyclerView.Adapter<MyCartAdapter.ViewHolder> {
    Context context;
    OnItemClickListener listener;
    List<ProductDetailResponse.Data> data = new ArrayList<>();
    List<Integer>totalData = new ArrayList<>();
    Map<ProductDetailResponse.Data, Integer> map = new HashMap<ProductDetailResponse.Data, Integer>();

    public MyCartAdapter(List<ProductDetailResponse.Data> data, Context context, OnItemClickListener listener) {
        this.context = context;
        this.listener = listener;
        //this.data = data;

        for (ProductDetailResponse.Data temp : data) {
            Integer count = map.get(temp);
            map.put(temp, (count == null) ? 1 : count + 1);
        }
        for (Map.Entry<ProductDetailResponse.Data,Integer>entry : map.entrySet()){
            this.data.add(entry.getKey());
            totalData.add(entry.getValue());
        }
        //this.dataAdded = dataAdded;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.holder_my_cart, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        final ProductDetailResponse.Data item = data.get(position);

        holder.textCount.setText(totalData.get(position)+"");
        Picasso.get().load(item.getImage()).fit().into(holder.imageProduct);
        holder.textNameProduct.setText(item.getName());
        holder.textNameTypeProduct.setText(item.getCategory_name());
        holder.textPrice.setText(TextFormatter.make(context).IDRFormat(item.getPrice() + ""));

        holder.imageMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onMinusClicked(item,position);
            }
        });
        holder.imagePlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onPlusClicked(item);
            }
        });
        holder.imageDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onTrashClicked(item,position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image_product)
        ImageView imageProduct;
        @BindView(R.id.image_delete)
        ImageView imageDelete;
        @BindView(R.id.text_name_type_product)
        TextView textNameTypeProduct;
        @BindView(R.id.text_name_product)
        TextView textNameProduct;
        @BindView(R.id.image_minus)
        ImageView imageMinus;
        @BindView(R.id.text_count)
        TextView textCount;
        @BindView(R.id.image_plus)
        ImageView imagePlus;
        @BindView(R.id.text_price)
        TextView textPrice;
        @BindView(R.id.rel_item)
        RelativeLayout relItem;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnItemClickListener {
        void onPlusClicked(ProductDetailResponse.Data item);

        void onMinusClicked(ProductDetailResponse.Data item,int position);

        void onTrashClicked(ProductDetailResponse.Data item,int position);
    }

}
