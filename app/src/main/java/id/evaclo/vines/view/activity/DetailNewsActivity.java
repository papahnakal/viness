package id.evaclo.vines.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.evaclo.vines.R;
import id.evaclo.vines_service.core.BaseActivity;
import id.evaclo.vines_service.model.news.NewsDetailResponse;
import id.evaclo.vines_service.model.news.NewsListResponse;
import id.evaclo.vines_service.presenter.NewsPresenter;
import id.evaclo.vines_service.service.VinesSession;
import id.evaclo.vines_service.view.NewsView;

public class DetailNewsActivity extends BaseActivity<NewsView,NewsPresenter> implements NewsView {

    @BindView(R.id.text_toolbar_title)
    TextView textToolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.image_detail_news)
    ImageView imageDetailNews;
    @BindView(R.id.webview_news)
    WebView webviewNews;
    VinesSession vinesSession;
    int id_news;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_news);
        Intent intent = getIntent();
        id_news = intent.getIntExtra("id_news",0);
        ButterKnife.bind(this);
        getMvpPresenter().getDetailNews(id_news);

    }

    @Override
    public NewsPresenter createPresenter() {
        vinesSession = new VinesSession(this);
        return new NewsPresenter(this,vinesSession);
    }

    public void setToolbar(String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        textToolbarTitle.setText(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.icons_navigation_arrow_left);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return true;
    }

    @Override
    public void failure(String message) {

    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void onSuccessGetListNews(NewsListResponse response) {

    }

    @Override
    public void onSuccessGetDetailtNews(NewsDetailResponse response) {
        setToolbar(response.getData().get(0).getTitle());
        Picasso.get().load(response.getData().get(0).getImage()).fit().into(imageDetailNews);
        webviewNews.getSettings().setDefaultFontSize(14);
        webviewNews.getSettings().setJavaScriptEnabled(true);
        webviewNews.loadDataWithBaseURL("", "<html><body  style=\"text-align:justify;\">"+response.getData().get(0).getDescription()+"</body></html>", "text/html", "UTF-8", "");
    }
}
