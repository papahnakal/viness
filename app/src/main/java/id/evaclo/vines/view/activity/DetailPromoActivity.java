package id.evaclo.vines.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.evaclo.vines.R;
import id.evaclo.vines_service.core.BaseActivity;
import id.evaclo.vines_service.model.promotion.PromotionListProductResponse;
import id.evaclo.vines_service.model.promotion.PromotionListResponse;
import id.evaclo.vines_service.presenter.PromotionPresenter;
import id.evaclo.vines_service.service.VinesSession;
import id.evaclo.vines_service.view.PromotionView;

public class DetailPromoActivity extends BaseActivity<PromotionView, PromotionPresenter> implements PromotionView {
    VinesSession vinesSession;
    int promotion_id;
    String banner;
    @BindView(R.id.text_toolbar_title)
    TextView textToolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.image_promo)
    ImageView imagePromo;
    @BindView(R.id.text_name_promo)
    TextView textNamePromo;
    @BindView(R.id.text_category)
    TextView textCategory;
    @BindView(R.id.text_detail_promotion)
    TextView textDetailPromotion;
    @BindView(R.id.button_shop_now)
    Button buttonShopNow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_promo);
        ButterKnife.bind(this);
        setToolbar();
        promotion_id = getIntent().getIntExtra("promotion_id", 0);
        banner = getIntent().getStringExtra("banner");
        getMvpPresenter().getDetailPromotion(promotion_id,10,0);

    }
    public void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.icons_navigation_arrow_left);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return true;
    }

    @Override
    public PromotionPresenter createPresenter() {
        vinesSession = new VinesSession(this);
        return new PromotionPresenter(this, vinesSession);
    }

    @Override
    public void failure(String message) {
        Toast.makeText(DetailPromoActivity.this,message,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(DetailPromoActivity.this,message,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccessGetListPromotion(PromotionListResponse response) {

    }

    @Override
    public void onSuccessGetDetailPromotion(PromotionListProductResponse response) {
        buttonShopNow.setVisibility(View.VISIBLE);
        PromotionListProductResponse.Data item = response.getData().get(0);
        textCategory.setText(item.getCategory_name());
        textDetailPromotion.setText(item.getSummary());
        textNamePromo.setText(item.getPromotion_title());
        textToolbarTitle.setText(item.getName());
        Picasso.get().load(banner).resize(300,200).into(imagePromo);
    }

    @OnClick(R.id.button_shop_now)
    public void onViewClicked() {
        Intent toStore = new Intent(DetailPromoActivity.this,StoreActivity.class);
        startActivity(toStore);
        finish();
    }
}
