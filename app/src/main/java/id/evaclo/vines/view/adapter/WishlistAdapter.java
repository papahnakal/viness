package id.evaclo.vines.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.evaclo.vines.R;
import id.evaclo.vines.utils.TextFormatter;
import id.evaclo.vines_service.model.wishlist.WishListResponse;

public class WishlistAdapter extends RecyclerView.Adapter<WishlistAdapter.ViewHolder> {
    Context context;
    OnItemClickListener listener;
    OnBuyClickListener  buyClickListener;
    List<WishListResponse.Data> data;

    public WishlistAdapter(List<WishListResponse.Data> data, Context context, OnItemClickListener listener,OnBuyClickListener  buyClickListener) {
        this.context = context;
        this.listener = listener;
        this.buyClickListener = buyClickListener;
        this.data = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.holder_wishlist, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final WishListResponse.Data item = data.get(position);
        holder.textDiscount.setText(item.getDiscount()+"%");
        holder.textPrice.setText(TextFormatter.make(context).IDRFormat(item.getPrice()+""));
        holder.textPriceStrikeout.setText("");
        holder.textProductName.setText(item.getName());
        holder.textProductType.setText(item.getCategory_name());
        Picasso.get().load(item.getImage()).fit().into(holder.imageProduct);
        holder.linBuyProd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buyClickListener.onBuyClicked(item);
            }
        });
        if(item.getDiscount()==0){
            holder.linDiscount.setVisibility(View.GONE);
        }else {
            holder.linDiscount.setVisibility(View.VISIBLE);
        }
        holder.image_love.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onLikeClicked(item);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image_product)
        ImageView imageProduct;
        @BindView(R.id.text_product_type)
        TextView textProductType;
        @BindView(R.id.text_product_name)
        TextView textProductName;
        @BindView(R.id.text_price_strikeout)
        TextView textPriceStrikeout;
        @BindView(R.id.text_price)
        TextView textPrice;
        @BindView(R.id.lin_buy_prod)
        LinearLayout linBuyProd;
        @BindView(R.id.text_discount_wishlist)
        TextView textDiscount;
        @BindView(R.id.lin_discount)
        LinearLayout linDiscount;
        @BindView(R.id.card_prod)
        CardView cardProd;
        @BindView(R.id.image_love)
        ImageView image_love;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnItemClickListener {
        void onItemClicked(WishListResponse.Data item);
        void onLikeClicked(WishListResponse.Data item);
    }

    public interface OnBuyClickListener{
        void onBuyClicked(WishListResponse.Data item);
    }

}
