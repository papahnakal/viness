package id.evaclo.vines.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.evaclo.vines.R;
import id.evaclo.vines.utils.TextFormatter;
import id.evaclo.vines.view.adapter.MyCartAdapter;
import id.evaclo.vines_service.model.product.ProductDetailResponse;
import id.evaclo.vines_service.service.VinesSession;

public class MyCartActivity extends AppCompatActivity implements MyCartAdapter.OnItemClickListener {

    @BindView(R.id.text_toolbar_title)
    TextView textToolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recycler_cart)
    RecyclerView recyclerCart;
    @BindView(R.id.total_price)
    TextView totalPrice;
    @BindView(R.id.button_proceed)
    Button buttonProceed;
    @BindView(R.id.lin_proceed)
    LinearLayout linProceed;

    LinearLayoutManager linearLayoutManager;
    List<ProductDetailResponse.Data> datas = new ArrayList<>();

    VinesSession vinesSession;
    String amount = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_cart);
        ButterKnife.bind(this);
        setToolbar();
        vinesSession = new VinesSession(this);
        if(vinesSession.getCart()!=null) {
            datas = vinesSession.getCart();
            initRecycleProduct(datas);
        }
        //initRecycleProduct();
    }

    public void setToolbar() {
        setSupportActionBar(toolbar);
        textToolbarTitle.setText("SHOPPING CART");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.icons_navigation_arrow_left);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    public void dataSet(List<ProductDetailResponse.Data> datas){
        //Log.d("tempt",map.toString());
        //initRecycleProduct(datas);
    }

    public void initRecycleProduct(List<ProductDetailResponse.Data> data) {
        int total = 0;
        for (int i = 0; i < data.size(); i++) {
            total = total +data.get(i).getPrice();
        }
        amount = total+"";
        totalPrice.setText(TextFormatter.make(this).IDRFormat(amount));
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerCart.setHasFixedSize(true);
        recyclerCart.setLayoutManager(linearLayoutManager);
        recyclerCart.setAdapter(new MyCartAdapter(data,this, this));
    }

    @OnClick(R.id.button_proceed)
    public void onViewClicked() {
        ArrayList<ProductDetailResponse.Data> newData = new ArrayList<>();
        for (int i = 0; i <datas.size() ; i++) {
            newData.add(datas.get(i));
        }
        Intent toCheckout = new Intent(MyCartActivity.this,CheckOutActivity.class);
        toCheckout.putParcelableArrayListExtra("data",newData);
        toCheckout.putExtra("amount",amount);
        startActivity(toCheckout);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                //savedListChart();

        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onPlusClicked(ProductDetailResponse.Data item) {
        datas.add(item);
        initRecycleProduct(datas);
        savedListChart();
        Toast.makeText(MyCartActivity.this,"One more product has been add to Cart",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onMinusClicked(ProductDetailResponse.Data item,int position) {
        datas.remove(position);
        initRecycleProduct(datas);
        savedListChart();
        Toast.makeText(MyCartActivity.this,"One product has been remove from Cart",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTrashClicked(ProductDetailResponse.Data item, int position) {
        /*for (int i = 0; i < datas.size(); i++) {
            if(datas.get(i).getProduct_id()==item.getProduct_id()){
                datas.remove(i);
            }
        }*/
        datas.removeAll(Arrays.asList(item));
        initRecycleProduct(datas);
        savedListChart();
    }

    public void savedListChart(){
        vinesSession.setCart(datas);
    }
}
