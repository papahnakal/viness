package id.evaclo.vines.view.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.evaclo.vines.R;
import id.evaclo.vines.utils.CLocation;
import id.evaclo.vines.utils.CircleTransform;
import id.evaclo.vines.utils.IBaseGpsListener;
import id.evaclo.vines.utils.MarkerPosition;
import id.evaclo.vines.view.adapter.PromotionAdapter;
import id.evaclo.vines_service.core.BaseActivity;
import id.evaclo.vines_service.model.promotion.PromotionListResponse;
import id.evaclo.vines_service.model.store.StoreListResponse;
import id.evaclo.vines_service.presenter.HomepagePresenter;
import id.evaclo.vines_service.service.VinesSession;
import id.evaclo.vines_service.view.HomepageView;

public class HomepageActivity extends BaseActivity<HomepageView, HomepagePresenter> implements PromotionAdapter.OnItemClickListener, HomepageView, NavigationView.OnNavigationItemSelectedListener, IBaseGpsListener, GoogleMap.OnCameraMoveStartedListener,
        GoogleMap.OnCameraMoveListener,
        GoogleMap.OnCameraMoveCanceledListener,
        GoogleMap.OnCameraIdleListener, OnMapReadyCallback {
    private static final int REQUEST_LOCATION = 100;
    static final int MY_PERMISSIONS_REQUEST_FINELOCATION = 1;
    static final int MY_PERMISSIONS_REQUEST_COARSELOCATION = 1;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.text_edit)
    TextView textEdit;
    @BindView(R.id.recycler_promo)
    RecyclerView recyclerPromo;
    @BindView(R.id.lin_seeAllStroe)
    LinearLayout linSeeAllStroe;
    @BindView(R.id.drawerleft)
    NavigationView drawerleft;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.container_view)
    FrameLayout containerView;
    @BindView(R.id.text_promo)
    TextView textPromo;
    @BindView(R.id.image_my_location)
    ImageView imageMyLocation;
    @BindView(R.id.rel_promo)
    RelativeLayout relPromo;
    @BindView(R.id.card_select)
    CardView cardSelect;

    private ActionBarDrawerToggle mDrawerToggle;
    private VinesSession session;
    ImageView img_profile;
    TextView text_name_profile, text_email_profile, text_phone_profile;
    LinearLayout lin_seeProfile;
    List<PromotionListResponse.Data> data;
    LinearLayoutManager linearLayoutManager;

    private GoogleMap mMap;
    private LatLng jakarta;
    double longitude;
    double latitude;
    private SupportMapFragment mapFragment;
    List<MarkerPosition> markerPositions = new ArrayList<>();
    List<StoreListResponse.Data> dataStore = new ArrayList<>();
    LocationManager lm;
    Location location;
    boolean first;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage);
        ButterKnife.bind(this);
        first = false;
        session = VinesSession.create(this);
        getMyLocation(null);
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if (session.getToken().equalsIgnoreCase("")) {
            Intent intentToLogin = new Intent(HomepageActivity.this, SignInActivity.class);
            startActivity(intentToLogin);
            finish();
        } else {
            drawerleft.setNavigationItemSelectedListener(this);
            Log.d("session", "toke : " + session.getToken());
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ico_nav_menubar);
            setTwoNavDrawer();
            setNavigationView(session);
            getMvpPresenter().getListPromotion(10);
            getMvpPresenter().getListStore(0.0, 0.0);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        first = false;
    }
/*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;

        //return super.onCreateOptionsMenu(menu);
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            drawerLayout.openDrawer(drawerleft);
        } else {
            return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    public void setNavigationView(VinesSession session) {
        View headerView = drawerleft.getHeaderView(0);
        img_profile = (ImageView) headerView.findViewById(R.id.image_name_profile);
        text_name_profile = (TextView) headerView.findViewById(R.id.text_name_profile);
        text_email_profile = (TextView) headerView.findViewById(R.id.text_email_profile);
        text_phone_profile = (TextView) headerView.findViewById(R.id.text_phone_profile);

        text_name_profile.setText(session.getLoginResponse().getData().getUser_data().get(0).getFullname());
        text_email_profile.setText(session.getLoginResponse().getData().getUser_data().get(0).getEmail());
        Picasso.get().load(session.getLoginResponse().getData().getUser_data().get(0).getFoto()).transform(new CircleTransform()).fit().centerCrop().into(img_profile);
        lin_seeProfile = (LinearLayout) headerView.findViewById(R.id.lin_seeProfile);

        headerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toPfrofile = new Intent(HomepageActivity.this, ProfileActivity.class);
                startActivity(toPfrofile);
            }
        });
        //text_name_profile.setText(session.getLoginResponse().);

    }

    public void setTwoNavDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.app_names, R.string.app_names) {
            @Override
            public void onDrawerClosed(View view) {
                drawerLayout.closeDrawer(drawerleft);
                supportInvalidateOptionsMenu();
                mDrawerToggle.syncState();
            }

            @Override
            public void onDrawerOpened(View view) {
                drawerLayout.openDrawer(drawerleft);
                supportInvalidateOptionsMenu();
                mDrawerToggle.syncState();
            }

            @Override
            public void onDrawerSlide(View view, float slideOffset) {
                super.onDrawerSlide(drawerleft, slideOffset);
                drawerLayout.bringChildToFront(drawerleft);
                drawerLayout.requestLayout();
            }
        };
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        drawerLayout.addDrawerListener(mDrawerToggle);

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        // We will provide our own zoom controls.
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        //createMarker(latitude,longitude);
        if (markerPositions.size() > 0) {
            for (int i = 0; i < markerPositions.size(); i++) {
                createMarker(markerPositions.get(i).getLat(), markerPositions.get(i).getLng(), dataStore.get(i)).showInfoWindow();
                if (i == 0) {
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(markerPositions.get(i).getLat(), markerPositions.get(i).getLng()), 15));
                }
                //mMap.addMarker(new MarkerOptions().position(new LatLng(markerPositions.get(i).getLat(), markerPositions.get(i).getLng())).title("vines"));
            }
            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    if (first) {
                        for (int i = 0; i < dataStore.size(); i++) {
                            if (dataStore.get(i) != null && dataStore.get(i).getName().equalsIgnoreCase(marker.getTitle())) {
                                Intent toStore = new Intent(HomepageActivity.this, HomepageStoreActivity.class);
                                toStore.putExtra("intent", dataStore.get(i));
                                startActivity(toStore);
                                first = false;
                                marker.hideInfoWindow();
                            }
                        }
                    } else {
                        first = true;
                    }
                    return false;
                }
            });
        }

        //mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 15));

        //mMap.animateCamera(CameraUpdateFactory.zoomIn());

    }


    public void getMyLocation(final Location location) {
        NumberFormat nf = NumberFormat.getInstance();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            // Check permission
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_LOCATION);
        } else {
            // startBeermay(); // <-- Start Beemray here
            lm = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
            lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
            //updateLocation(null);
            if (location != null) {
                //location.setUseMetricunits(false);
                latitude = location.getLatitude();
                longitude = location.getLongitude();
                if (markerPositions.size() > 0) {
                    markerPositions.set(0, new MarkerPosition(latitude, longitude));
                } else {
                    markerPositions.add(0, new MarkerPosition(latitude, longitude));
                }
                //mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 15));
                Log.d("LATLNG", "longi : " + longitude + " || latitude : " + latitude);
                mapFragment.getMapAsync(this);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_LOCATION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                return;
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    protected Marker createMarker(double latitude, double longitude, StoreListResponse.Data data) {
        if (data != null) {
            return mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(latitude, longitude))
                    .anchor(0.5f, 0.5f)
                    .title(data.getName())
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ico_pinpoint_2))
            );
        } else {
            return mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(latitude, longitude))
                    .anchor(0.5f, 0.5f));
        }
        //.snippet(snippet);
        // .icon(BitmapDescriptorFactory.fromResource(iconResID)));
    }

    @Override
    public void onCameraMoveCanceled() {

    }

    @Override
    public void onCameraMove() {

    }

    @Override
    public void onCameraMoveStarted(int i) {

    }

    @Override
    public void onCameraIdle() {

    }

    @OnClick(R.id.lin_seeAllStroe)
    public void onViewClicked() {
        Intent toAllstore = new Intent(HomepageActivity.this, StoreActivity.class);
        startActivity(toAllstore);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {

            case R.id.nav_wishlist:
                //do somthing
                Intent intent = new Intent(HomepageActivity.this, WishListActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_news:
                Intent toNews = new Intent(HomepageActivity.this, NewsActivity.class);
                startActivity(toNews);
                break;
            /*case R.id.nav_about:
                Intent toAbout = new Intent(HomepageActivity.this, SupportActivity.class);
                startActivity(toAbout);
                break;*/
            case R.id.nav_support:
                Intent toSupport = new Intent(HomepageActivity.this, SupportActivity.class);
                startActivity(toSupport);
                break;
            case R.id.nav_party:
                Intent toParty = new Intent(HomepageActivity.this, PartyServiceActivity.class);
                startActivity(toParty);
                break;
            case R.id.nav_promotion:
                Intent toPromotion = new Intent(HomepageActivity.this, PromotionActivity.class);
                startActivity(toPromotion);
                break;
            case R.id.nav_retail:
                Intent toStore = new Intent(HomepageActivity.this, StoreActivity.class);
                startActivity(toStore);

        }
        //close navigation drawer
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @OnClick(R.id.text_promo)
    public void onPromoClicked() {
        Intent toPromo = new Intent(HomepageActivity.this, PromotionActivity.class);
        startActivity(toPromo);
    }

    @Override
    public HomepagePresenter createPresenter() {
        session = new VinesSession(this);
        return new HomepagePresenter(this, session);
    }

    public void initrecycleview(PromotionListResponse response) {
        data = response.getData();
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerPromo.setHasFixedSize(true);
        recyclerPromo.setNestedScrollingEnabled(false);
        recyclerPromo.setLayoutManager(linearLayoutManager);
        recyclerPromo.setAdapter(new PromotionAdapter(true, this, data, this));
    }

    @Override
    public void failure(String message) {

    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void onSuccessGetListPromotion(PromotionListResponse response) {
        initrecycleview(response);
    }

    @Override
    public void onSuccessGetListStore(StoreListResponse response) {
        markerPositions.add(new MarkerPosition(latitude, longitude));
        dataStore.add(null);
        for (int i = 0; i < response.getData().size(); i++) {
            markerPositions.add(new MarkerPosition(response.getData().get(i).getLatitude(), response.getData().get(i).getLongitude()));
            dataStore.add(response.getData().get(i));
        }
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onItemClicked(PromotionListResponse.Data item) {
        Intent toDetailPromo = new Intent(HomepageActivity.this, DetailPromoActivity.class);
        toDetailPromo.putExtra("promotion_id", item.getPromotion_id());
        toDetailPromo.putExtra("banner", item.getImage());
        startActivity(toDetailPromo);
    }

    @OnClick(R.id.text_edit)
    public void onMoreClicked() {
        Intent toPromo = new Intent(HomepageActivity.this, PromotionActivity.class);
        startActivity(toPromo);
    }

    private double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        return (dist);
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            Log.d("distance", "distance : " + distance(latitude, longitude, location.getLatitude(), location.getLongitude()));
            if (latitude == 0.0 || longitude == 0.0 || distance(latitude, longitude, location.getLatitude(), location.getLongitude()) > 1000) {
                if (markerPositions.size() > 0) {
                    markerPositions.remove(0);
                }
                CLocation myLocation = new CLocation(location, true);
                this.getMyLocation(location);
            }
        }
    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onGpsStatusChanged(int event) {

    }

    @OnClick(R.id.image_my_location)
    public void onMyLocation() {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(markerPositions.get(0).getLat(), markerPositions.get(0).getLng()), 15));
    }

    @OnClick(R.id.card_select)
    public void onCardClicked() {
        Intent toAllstore = new Intent(HomepageActivity.this, StoreActivity.class);
        startActivity(toAllstore);
    }
}
