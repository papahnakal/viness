package id.evaclo.vines.view.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.evaclo.vines.R;
import id.evaclo.vines_service.core.BaseActivity;
import id.evaclo.vines_service.model.TermResponse;
import id.evaclo.vines_service.model.about.AboutResponse;
import id.evaclo.vines_service.model.contact.ContactResponse;
import id.evaclo.vines_service.presenter.AboutPresenter;
import id.evaclo.vines_service.service.VinesSession;
import id.evaclo.vines_service.view.AboutView;

public class AboutActivity extends BaseActivity<AboutView,AboutPresenter>implements AboutView {

    @BindView(R.id.text_toolbar_title)
    TextView textToolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.webview)
    WebView webview;
    VinesSession vinesSession;
    String from;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        ButterKnife.bind(this);
        setToolbar();
        from = getIntent().getStringExtra("from");
        if(from.equalsIgnoreCase("privacy")){
            getMvpPresenter().postGetPrivacy();
        }else if(from.equalsIgnoreCase("term")){
            getMvpPresenter().postGetTerm();
        }else {
            getMvpPresenter().getAbout();
        }
    }
    public void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //textToolbarTitle.setText("ABOUT");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.icons_navigation_arrow_left);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return true;
    }

    @Override
    public AboutPresenter createPresenter() {
        vinesSession = new VinesSession(this);
        return new AboutPresenter(this,vinesSession);
    }

    @Override
    public void failure(String message) {

    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void onSuccessGetAbout(AboutResponse response) {
        textToolbarTitle.setText("ABOUT");
        webview.loadDataWithBaseURL("",response.getData().get(0).getDescription(),"text/html","UTF-8","");
    }

    @Override
    public void onSuccessGetTerm(TermResponse response) {
        textToolbarTitle.setText("TERM AND CONDITION");
        webview.loadDataWithBaseURL("",response.getData().get(0).getDescription(),"text/html","UTF-8","");
    }

    @Override
    public void onSuccessGetPrivateInfo(TermResponse response) {
        textToolbarTitle.setText("PRIVACY POLICY");
        webview.loadDataWithBaseURL("",response.getData().get(0).getDescription(),"text/html","UTF-8","");
    }

    @Override
    public void onSuccessPostCoomment(ContactResponse response) {

    }
}
