package id.evaclo.vines.view.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.evaclo.vines.R;
import id.evaclo.vines_service.core.BaseActivity;
import id.evaclo.vines_service.model.login.UpdatePasswordResponse;
import id.evaclo.vines_service.presenter.UpdatePasswordPresenter;
import id.evaclo.vines_service.service.VinesSession;
import id.evaclo.vines_service.view.UpdatePasswordView;

public class ChangePasswordActivity extends BaseActivity<UpdatePasswordView, UpdatePasswordPresenter> implements UpdatePasswordView {

    @BindView(R.id.text_toolbar_title)
    TextView textToolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.edit_old_password)
    EditText editOldPassword;
    @BindView(R.id.edit_new_password)
    EditText editNewPassword;
    @BindView(R.id.edit_re_enter_old_password)
    EditText editReEnterOldPassword;
    @BindView(R.id.button_save)
    Button buttonSave;
    VinesSession vinesSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);
        setToolbar();
    }
    public void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        textToolbarTitle.setText("CHANGE PASSWORD");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.icons_navigation_arrow_left);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return true;
    }

    @Override
    public UpdatePasswordPresenter createPresenter() {
        vinesSession = new VinesSession(this);
        return new UpdatePasswordPresenter(this, vinesSession);
    }

    @Override
    public void onSuccessUpdatePassword(UpdatePasswordResponse response) {
        Toast.makeText(ChangePasswordActivity.this,response.getDisplay_message(),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure(String message) {
        Toast.makeText(ChangePasswordActivity.this,message,Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.button_save)
    public void onViewClicked() {
        if(!editNewPassword.getText().toString().equalsIgnoreCase(editReEnterOldPassword.getText().toString())){
            Toast.makeText(ChangePasswordActivity.this,"Password didn't match",Toast.LENGTH_SHORT).show();
        }else {
            getMvpPresenter().updatePassword(vinesSession.getToken(), editNewPassword.getText().toString(), vinesSession.getLoginResponse().getData().getUser_data().get(0).getEmail());
        }
    }
}
