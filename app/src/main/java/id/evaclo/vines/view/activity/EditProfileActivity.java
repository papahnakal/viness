package id.evaclo.vines.view.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.evaclo.vines.R;
import id.evaclo.vines_service.core.BaseActivity;
import id.evaclo.vines_service.model.login.EditProfileResponse;
import id.evaclo.vines_service.presenter.EditProfilePresenter;
import id.evaclo.vines_service.service.VinesSession;
import id.evaclo.vines_service.view.EditProfileView;

public class EditProfileActivity extends BaseActivity<EditProfileView, EditProfilePresenter> implements EditProfileView, DatePickerDialog.OnDateSetListener {
    private static final int SELECT_IMAGE = 0;
    @BindView(R.id.text_toolbar_title)
    TextView textToolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.lin_upload_picture)
    LinearLayout linUploadPicture;
    @BindView(R.id.edit_first_name)
    EditText editFirstName;
    @BindView(R.id.edit_last_name)
    EditText editLastName;
    @BindView(R.id.edit_email)
    EditText editEmail;
    @BindView(R.id.edit_address)
    EditText editAddress;
    @BindView(R.id.edit_phone)
    EditText editPhone;
    @BindView(R.id.button_update)
    Button buttonUpdate;
    @BindView(R.id.image_profile)
    ImageView imageProfile;

    VinesSession vinesSession;
    String date;
    //Uri src;
    File images;
    @BindView(R.id.edit_date)
    EditText editDate;
    @BindView(R.id.edit_month)
    EditText editMonth;
    @BindView(R.id.edit_years)
    EditText editYears;

    private int mYear, mMonth, mDay;
    Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);
        setToolbar();
        setFromSession();
    }

    public void setFromSession(){
        vinesSession = new VinesSession(this);
        Picasso.get().load(vinesSession.getLoginResponse().getData().getUser_data().get(0).getFoto()).into(imageProfile);
        if(vinesSession.getLoginResponse().getData().getUser_data().get(0).getFullname().split("\\s+").length>1){
            String[] splitStr = vinesSession.getLoginResponse().getData().getUser_data().get(0).getFullname().split("\\s+");
            editFirstName.setText(splitStr[0]);
            editLastName.setText(splitStr[1]);
        }else {
            editFirstName.setText(vinesSession.getLoginResponse().getData().getUser_data().get(0).getFullname());
        }
        editEmail.setText(vinesSession.getLoginResponse().getData().getUser_data().get(0).getEmail());
        editAddress.setText("Jakarta");
        editPhone.setText(vinesSession.getLoginResponse().getData().getUser_data().get(0).getPhone());
        String[] dates = vinesSession.getLoginResponse().getData().getUser_data().get(0).getDob().split("-");
        editDate.setText(dates[2]);
        editMonth.setText(dates[1]);
        editYears.setText(dates[0]);
        date = vinesSession.getLoginResponse().getData().getUser_data().get(0).getDob();
    }

    @OnClick(R.id.button_update)
    public void onViewClicked() {
        bitmap = ((BitmapDrawable)imageProfile.getDrawable()).getBitmap();
        getMvpPresenter().updateProfile(vinesSession.getLoginResponse().getData().getUser_data().get(0).getUser_id() + "",
                editFirstName.getText().toString(), editLastName.getText().toString(), editEmail.getText().toString(),
                editPhone.getText().toString(), editAddress.getText().toString(), date,
                vinesSession.getToken(), bitmap);
    }

    public void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        textToolbarTitle.setText("EDIT PROFILE");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.icons_navigation_arrow_left);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return true;
    }

    public void openGalery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_IMAGE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), data.getData());
                        imageProfile.setImageBitmap(bitmap);
                        //Uri src = getImageUri(getApplicationContext(), bitmap);
                        //images = new File(getRealPathFromURI(src));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(EditProfileActivity.this, "Canceled", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date());
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "IMG_" + timeStamp, null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    @OnClick(R.id.lin_upload_picture)
    public void onSelectImage() {
        openGalery();
    }

    @Override
    public EditProfilePresenter createPresenter() {
        vinesSession = new VinesSession(this);
        return new EditProfilePresenter(this, vinesSession);
    }

    @Override
    public void onSuccessEditProfile(EditProfileResponse response) {
        Toast.makeText(EditProfileActivity.this, response.getDisplay_message(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure(String message) {
        Toast.makeText(EditProfileActivity.this, message, Toast.LENGTH_SHORT).show();
    }

    @OnClick({R.id.edit_date, R.id.edit_month, R.id.edit_years})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.edit_date:
                showDatePicker();
                break;
            case R.id.edit_month:
                showDatePicker();
                break;
            case R.id.edit_years:
                showDatePicker();
                break;
        }
    }

    private void showDatePicker() {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(EditProfileActivity.this, R.style.DialogTheme,
                this, mYear, mMonth, mDay);

        datePickerDialog.show();

    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        String month = String.valueOf(monthOfYear);
        String day = String.valueOf(dayOfMonth);
        day = String.valueOf(dayOfMonth);
        if (dayOfMonth < 10) {
            day = String.valueOf("0" + dayOfMonth);
        }
        month = String.valueOf(monthOfYear + 1);
        if (monthOfYear < 9) {
            month = String.valueOf("0" + (monthOfYear + 1));
        }

        date = year + "-" + month + "-" + day;
        editDate.setText(day);
        editMonth.setText(month);
        editYears.setText(year+"");
    }
}
