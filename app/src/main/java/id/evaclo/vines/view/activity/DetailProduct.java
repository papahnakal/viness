package id.evaclo.vines.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.evaclo.vines.R;
import id.evaclo.vines.utils.TextFormatter;
import id.evaclo.vines.view.adapter.SpecialOfferAdapter;
import id.evaclo.vines_service.core.BaseActivity;
import id.evaclo.vines_service.model.product.ProductDetailResponse;
import id.evaclo.vines_service.model.product.ProductFavoriteListResponse;
import id.evaclo.vines_service.model.product.ProductListResponse;
import id.evaclo.vines_service.model.product.ProductSimilarListResponse;
import id.evaclo.vines_service.presenter.ProductPresenter;
import id.evaclo.vines_service.service.VinesSession;
import id.evaclo.vines_service.view.ProductView;

public class DetailProduct extends BaseActivity<ProductView, ProductPresenter> implements ProductView, SpecialOfferAdapter.OnItemClickListener {

    GridLayoutManager gridLayoutManager;
    @BindView(R.id.text_toolbar_title)
    TextView textToolbarTitle;
    @BindView(R.id.img_cart_shop)
    ImageView imgCartShop;
    @BindView(R.id.toolbar_product)
    Toolbar toolbarProduct;
    @BindView(R.id.line)
    LinearLayout line;
    @BindView(R.id.image_product)
    ImageView imageProduct;
    @BindView(R.id.rel_image_label)
    RelativeLayout relImageLabel;
    @BindView(R.id.image_favorite)
    ImageView imageFavorite;
    @BindView(R.id.text_product_type)
    TextView textProductType;
    @BindView(R.id.text_product_name)
    TextView textProductName;
    @BindView(R.id.text_price_strikeout)
    TextView textPriceStrikeout;
    @BindView(R.id.text_price)
    TextView textPrice;
    @BindView(R.id.text_description)
    TextView textDescription;
    @BindView(R.id.text_regions)
    TextView textRegions;
    @BindView(R.id.text_size)
    TextView textSize;
    @BindView(R.id.text_abv)
    TextView textAbv;
    @BindView(R.id.text_stock)
    TextView textStock;
    @BindView(R.id.recycler_similiar)
    RecyclerView recyclerSimiliar;
    @BindView(R.id.lin_add_item)
    LinearLayout linAddItem;
    VinesSession vinesSession;
    int productid;
    int store_id;
    String storeId;
    @BindView(R.id.text_discount)
    TextView textDiscount;
    List<ProductDetailResponse.Data> data = new ArrayList<>();
    @BindView(R.id.button_buy)
    Button buttonBuy;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.rel_botnav)
    RelativeLayout relBotnav;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_product);
        ButterKnife.bind(this);
        productid = getIntent().getIntExtra("productid", 0);
        storeId = getIntent().getStringExtra("storeId");
        store_id = getIntent().getIntExtra("store_id", 0);
        setToolbar();
        //initRecycleSpecialOffers();

        getMvpPresenter().getDetailProduct(storeId, productid);
        Log.d("detail_product", "storeId:" + storeId + "||" + "prod_id:" + productid);
    }

    public void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_product);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.icons_navigation_arrow_left);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return true;
    }

    public void initRecycleSimilar(ProductSimilarListResponse response) {
        List<ProductSimilarListResponse.Data> similar = response.getData();
        List<ProductListResponse.Data> data = new ArrayList<>();
        for (int i = 0; i <response.getData().size() ; i++) {
            data.add(
                    new ProductListResponse.Data(
                            similar.get(i).getDatecreate(),
                            false,
                            similar.get(i).getCategory_name(),
                            similar.get(i).getImage(),
                            similar.get(i).getAbv(),
                            similar.get(i).getDiscount(),
                            0,
                            similar.get(i).getPrice(),
                            similar.get(i).getSummary(),
                            similar.get(i).getCode(),
                            similar.get(i).getName(),
                            similar.get(i).getCategory_id(),
                            similar.get(i).getProduct_id())
            );
        }
        gridLayoutManager = new GridLayoutManager(this, 2);
        recyclerSimiliar.setHasFixedSize(true);
        recyclerSimiliar.setLayoutManager(gridLayoutManager);
        recyclerSimiliar.setAdapter(new SpecialOfferAdapter(data,this, this));

    }

    /*@Override
    public void onSpecialOfferItemClicked() {

    }*/

    public void setDetail(ProductDetailResponse response) {
        data = response.getData();
        textAbv.setText(response.getData().get(0).getAbv() + "");
        textDescription.setText(response.getData().get(0).getSummary());
        textPrice.setText(TextFormatter.make(this).IDRFormat(response.getData().get(0).getPrice() + ""));
        textProductName.setText(response.getData().get(0).getName());
        textProductType.setText(response.getData().get(0).getCategory_name());
        textRegions.setText(response.getData().get(0).getSub_region());
        textSize.setText(response.getData().get(0).getSize().get(0).getStock() + "");
        textStock.setText(response.getData().get(0).getStock() + "");
        textToolbarTitle.setText(response.getData().get(0).getCategory_name());
        if (response.getData().get(0).getDiscount() > 0) {
            textDiscount.setText(response.getData().get(0).getDiscount() + "%");
        } else {
            relImageLabel.setVisibility(View.GONE);
        }
        Picasso.get().load(response.getData().get(0).getImage()).fit().centerCrop().into(imageProduct);

    }

    @Override
    public ProductPresenter createPresenter() {
        vinesSession = new VinesSession(this);
        return new ProductPresenter(this, vinesSession);
    }

    @Override
    public void failure(String message) {

    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void onSuccessGetListProduct(ProductListResponse response) {

    }

    @Override
    public void onSuccessGetSimilarProduct(ProductSimilarListResponse response) {
        initRecycleSimilar(response);
    }

    @Override
    public void onSuccessGetFavoriteProduct(ProductFavoriteListResponse response) {

    }

    @Override
    public void onSuccessGetDetailProduct(ProductDetailResponse response) {
        setDetail(response);
        getMvpPresenter().getSimilarProduct(vinesSession.getLoginResponse().getData().getUser_data().get(0).getUser_id(),0,10,response.getData().get(0).getCategory_name(),response.getData().get(0).getCategory_id(),store_id);
    }

    @Override
    public void onSuccessAddWishlist(String message) {
        Toast.makeText(DetailProduct.this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSpecialOfferItemClicked(ProductListResponse.Data item) {

    }

    @Override
    public void onAddToCartClicked(ProductListResponse.Data item) {
        List<ProductDetailResponse.Data> newCart = new ArrayList<>();

        if (vinesSession.getCart() != null) {
            newCart = vinesSession.getCart();
        }
        newCart.add(data.get(0));
        vinesSession.setCart(newCart);
        Toast.makeText(DetailProduct.this, "Product has been add to Cart", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.img_cart_shop)
    public void onViewClicked() {
        Intent intentToCart = new Intent(DetailProduct.this, MyCartActivity.class);
        startActivity(intentToCart);
    }

    @OnClick(R.id.lin_add_item)
    public void onAddToCart() {
        List<ProductDetailResponse.Data> newCart = new ArrayList<>();

        if (vinesSession.getCart() != null) {
            newCart = vinesSession.getCart();
        }
        newCart.add(data.get(0));
        vinesSession.setCart(newCart);
        Toast.makeText(DetailProduct.this, "Product has been add to Cart", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.image_favorite)
    public void onFavoriteClicked() {
        imageFavorite.setImageResource(R.drawable.ico_nav_favorite_full);
        getMvpPresenter().addWishList(vinesSession.getToken(), data.get(0).getProduct_id(), vinesSession.getLoginResponse().getData().getUser_data().get(0).getUser_id(), store_id);
    }

    @OnClick(R.id.button_buy)
    public void onBuy() {
        List<ProductDetailResponse.Data> newCart = new ArrayList<>();

        if (vinesSession.getCart() != null) {
            newCart = vinesSession.getCart();
        }
        newCart.add(data.get(0));
        vinesSession.setCart(newCart);
        Intent intentToCart = new Intent(DetailProduct.this, MyCartActivity.class);
        startActivity(intentToCart);
    }

    @OnClick(R.id.fab)
    public void onFabClicked() {
        Intent intentToCart = new Intent(DetailProduct.this, MyCartActivity.class);
        startActivity(intentToCart);
    }
}
