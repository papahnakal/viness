package id.evaclo.vines.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.evaclo.vines.R;
import id.evaclo.vines_service.view.AboutView;

public class SupportActivity extends AppCompatActivity {

    @BindView(R.id.text_toolbar_title)
    TextView textToolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rel_contactus)
    RelativeLayout relContactus;
    @BindView(R.id.rel_privacy)
    RelativeLayout relPrivacy;
    @BindView(R.id.rel_terms)
    RelativeLayout relTerms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support);
        ButterKnife.bind(this);
        setToolbar();
    }

    public void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        textToolbarTitle.setText("SUPPORT");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.icons_navigation_arrow_left);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return true;
    }

    @OnClick({R.id.rel_contactus, R.id.rel_privacy, R.id.rel_terms})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rel_contactus:
                Intent toContact = new Intent(SupportActivity.this,ContactUsActivity.class);
                startActivity(toContact);
                break;
            case R.id.rel_privacy:
                Intent toPrivacy = new Intent(SupportActivity.this,AboutActivity.class);
                toPrivacy.putExtra("from","privacy");
                startActivity(toPrivacy);
                break;
            case R.id.rel_terms:
                Intent toTerm = new Intent(SupportActivity.this,AboutActivity.class);
                toTerm.putExtra("from","term");
                startActivity(toTerm);
                break;
        }
    }
}
