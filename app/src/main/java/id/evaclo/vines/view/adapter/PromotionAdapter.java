package id.evaclo.vines.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.evaclo.vines.R;
import id.evaclo.vines_service.model.promotion.PromotionListResponse;

public class PromotionAdapter extends RecyclerView.Adapter<PromotionAdapter.ViewHolder> {
    Context context;
    List<PromotionListResponse.Data> data;
    OnItemClickListener listener;
    boolean isHome;

    public PromotionAdapter(boolean isHome, Context context, List<PromotionListResponse.Data> data, OnItemClickListener listener) {
        this.context = context;
        this.data = data;
        this.listener = listener;
        this.isHome = isHome;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.holder_promotion, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final PromotionListResponse.Data item = data.get(position);
        if (isHome) {
            Picasso.get().load(item.getImage()).resize(300, 180).into(holder.imagePromo);
        }else {
            holder.imagePromo.setAdjustViewBounds(true);
            Picasso.get().load(item.getImage()).into(holder.imagePromo);
        }
        holder.imagePromo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClicked(item);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image_promo)
        ImageView imagePromo;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
    public interface OnItemClickListener{
        void onItemClicked(PromotionListResponse.Data item);
    }
}
