package id.evaclo.vines.view.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.evaclo.vines.R;
import id.evaclo.vines_service.core.BaseActivity;
import id.evaclo.vines_service.core.BaseView;
import id.evaclo.vines_service.model.TermResponse;
import id.evaclo.vines_service.model.about.AboutResponse;
import id.evaclo.vines_service.model.contact.ContactResponse;
import id.evaclo.vines_service.presenter.AboutPresenter;
import id.evaclo.vines_service.service.VinesSession;
import id.evaclo.vines_service.view.AboutView;

public class ContactUsActivity extends BaseActivity<AboutView,AboutPresenter>implements AboutView {

    @BindView(R.id.text_toolbar_title)
    TextView textToolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.edit_first_name)
    EditText editFirstName;
    @BindView(R.id.edit_email_address)
    EditText editEmailAddress;
    @BindView(R.id.edit_phone)
    EditText editPhone;
    @BindView(R.id.edit_message)
    EditText editMessage;
    @BindView(R.id.button_send_message)
    Button buttonSendMessage;
    VinesSession vinesSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        ButterKnife.bind(this);
        setToolbar();
    }

    public void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        textToolbarTitle.setText("CONTACT US");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.icons_navigation_arrow_left);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return true;
    }

    @OnClick(R.id.button_send_message)
    public void onViewClicked() {
        getMvpPresenter().postContact(editEmailAddress.getText().toString(),editPhone.getText().toString(),editMessage.getText().toString());
    }

    @Override
    public AboutPresenter createPresenter() {
        vinesSession = new VinesSession(this);
        return new AboutPresenter(this,vinesSession);
    }

    @Override
    public void failure(String message) {

    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void onSuccessGetAbout(AboutResponse response) {

    }

    @Override
    public void onSuccessGetTerm(TermResponse response) {

    }

    @Override
    public void onSuccessGetPrivateInfo(TermResponse response) {

    }

    @Override
    public void onSuccessPostCoomment(ContactResponse response) {
        Toast.makeText(ContactUsActivity.this,response.getData().getMessage(),Toast.LENGTH_SHORT).show();
    }
}
