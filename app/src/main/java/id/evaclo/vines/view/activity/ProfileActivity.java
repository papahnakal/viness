package id.evaclo.vines.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.evaclo.vines.R;
import id.evaclo.vines.utils.CircleTransform;
import id.evaclo.vines.view.adapter.RecentOrderAdapter;
import id.evaclo.vines_service.core.BaseActivity;
import id.evaclo.vines_service.model.order.RecentOrderResponse;
import id.evaclo.vines_service.presenter.ProfilePresenter;
import id.evaclo.vines_service.service.VinesSession;
import id.evaclo.vines_service.view.ProfileView;

public class ProfileActivity extends BaseActivity<ProfileView, ProfilePresenter>implements ProfileView,RecentOrderAdapter.OnItemClickListener {

    @BindView(R.id.text_toolbar_title)
    TextView textToolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.image_profile)
    ImageView imageProfile;
    @BindView(R.id.text_name_profile)
    TextView textNameProfile;
    @BindView(R.id.text_email)
    TextView textEmail;
    @BindView(R.id.text_number_phone)
    TextView textNumberPhone;
    @BindView(R.id.text_edit)
    TextView textEdit;
    @BindView(R.id.image_points)
    ImageView imagePoints;
    @BindView(R.id.text_point)
    TextView textPoint;
    @BindView(R.id.image_orders)
    ImageView imageOrders;
    @BindView(R.id.text_all)
    TextView textAll;
    @BindView(R.id.recycle_orders)
    RecyclerView recycleOrders;
    @BindView(R.id.image_password)
    ImageView imagePassword;
    @BindView(R.id.image_rate)
    ImageView imageRate;
    @BindView(R.id.image_logout)
    ImageView imageLogout;
    VinesSession vinesSession;
    @BindView(R.id.rel_changepass)
    RelativeLayout relChangepass;
    @BindView(R.id.rel_log_out)
    RelativeLayout relLogOut;

    List<RecentOrderResponse.Data> data = new ArrayList<>();
    LinearLayoutManager linearLayoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        vinesSession = new VinesSession(this);
        textPoint.setText(vinesSession.getLoginResponse().getData().getUser_data().get(0).getPoint()+" POINT");
        textEmail.setText(vinesSession.getLoginResponse().getData().getUser_data().get(0).getEmail());
        textNameProfile.setText(vinesSession.getLoginResponse().getData().getUser_data().get(0).getFullname());
        Picasso.get().load(vinesSession.getLoginResponse().getData().getUser_data().get(0).getFoto()).transform(new CircleTransform()).fit().centerCrop().into(imageProfile);
        //textNumberPhone.setText(vinesSession.getLoginResponse().getData().getUser_data().get(0).get);
        setToolbar("PROFILE");
        getMvpPresenter().getRecentOrder(vinesSession.getToken(),0,10,vinesSession.getLoginResponse().getData().getUser_data().get(0).getUser_id());
    }

    public void setToolbar(String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        textToolbarTitle.setText(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.icons_navigation_arrow_left);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    public void initrecycleview(RecentOrderResponse response){
        data = response.getData();
        linearLayoutManager = new LinearLayoutManager(this);
        recycleOrders.setHasFixedSize(true);
        recycleOrders.setNestedScrollingEnabled(false);
        recycleOrders.setLayoutManager(linearLayoutManager);
        recycleOrders.setAdapter(new RecentOrderAdapter(data,this,this));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return true;
    }

    @OnClick(R.id.text_all)
    public void onAllClicked() {
        Intent toOrders = new Intent(ProfileActivity.this, AllOrderActivity.class);
        startActivity(toOrders);
    }

    @OnClick(R.id.rel_changepass)
    public void onChangePass() {
        Intent toChangePassword = new Intent(ProfileActivity.this, ChangePasswordActivity.class);
        startActivity(toChangePassword);
    }

    @OnClick({R.id.image_profile, R.id.rel_log_out})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_profile:
                break;
            case R.id.rel_log_out:
                vinesSession.clear();
                Intent toLogin = new Intent(ProfileActivity.this, SignInActivity.class);
                startActivity(toLogin);
                finish();
                break;
        }
    }

    @OnClick(R.id.text_edit)
    public void onEditProfile() {
        Intent toChangePassword = new Intent(ProfileActivity.this, EditProfileActivity.class);
        startActivity(toChangePassword);
    }

    @Override
    public ProfilePresenter createPresenter() {
        vinesSession = new VinesSession(this);
        return new ProfilePresenter(this,vinesSession);
    }

    @Override
    public void onSuccessGetRecentOrder(RecentOrderResponse response) {
        initrecycleview(response);
    }

    @Override
    public void failure(String message) {

    }

    @Override
    public void onItemClicked(RecentOrderResponse.Data data) {

    }
}
