package id.evaclo.vines.view.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.evaclo.vines.R;

public class FogetPasswordActivity extends AppCompatActivity {

    @BindView(R.id.toolabar)
    Toolbar toolabar;
    @BindView(R.id.button_submit)
    Button buttonSubmit;
    @BindView(R.id.edit_email)
    EditText editEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_foget_password);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.button_submit)
    public void onViewClicked() {
    }
}
