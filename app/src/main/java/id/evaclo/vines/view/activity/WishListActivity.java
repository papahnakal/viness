package id.evaclo.vines.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.evaclo.vines.R;
import id.evaclo.vines.view.adapter.SpecialOfferAdapter;
import id.evaclo.vines.view.adapter.WishlistAdapter;
import id.evaclo.vines_service.core.BaseActivity;
import id.evaclo.vines_service.model.product.ProductDetailResponse;
import id.evaclo.vines_service.model.product.ProductListResponse;
import id.evaclo.vines_service.model.wishlist.WishListResponse;
import id.evaclo.vines_service.presenter.WishListPresenter;
import id.evaclo.vines_service.service.VinesSession;
import id.evaclo.vines_service.view.WishlistView;

public class WishListActivity extends BaseActivity<WishlistView,WishListPresenter> implements WishlistView,WishlistAdapter.OnItemClickListener,WishlistAdapter.OnBuyClickListener {

    @BindView(R.id.text_toolbar_title)
    TextView textToolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recycle_wishlist)
    RecyclerView recycleWishlist;
    List<WishListResponse.Data> data;
    GridLayoutManager gridLayoutManager;
    VinesSession vinesSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wish_list);
        ButterKnife.bind(this);
        setToolbar();
        vinesSession = new VinesSession(this);
        getMvpPresenter().getListWishlist(vinesSession.getToken(),0,10,vinesSession.getLoginResponse().getData().getUser_data().get(0).getUser_id());
    }
    public void setToolbar() {
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_product);
        setSupportActionBar(toolbar);
        textToolbarTitle.setText("WISHLIST");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.icons_navigation_arrow_left);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return true;
    }
    public void initRecycleProduct(WishListResponse response) {
        data = response.getData();
        gridLayoutManager = new GridLayoutManager(this, 2);
        recycleWishlist.setHasFixedSize(true);
        recycleWishlist.setLayoutManager(gridLayoutManager);
        recycleWishlist.setAdapter(new WishlistAdapter(data,this, this,this));
    }

    @Override
    public void onItemClicked(WishListResponse.Data item) {
        Intent a = new Intent(WishListActivity.this,DetailProduct.class);
        a.putExtra("productid",item.getProduct_id());
        a.putExtra("storeId","");
        startActivity(a);
    }

    @Override
    public void onLikeClicked(WishListResponse.Data item) {
        getMvpPresenter().addWishList(vinesSession.getToken(),item.getProduct_id(),vinesSession.getLoginResponse().getData().getUser_data().get(0).getUser_id(),item.getStore_id());
    }

    @Override
    public WishListPresenter createPresenter() {
        vinesSession = new VinesSession(this);
        return new WishListPresenter(this,vinesSession);
    }

    @Override
    public void onSuccessGetWishList(WishListResponse response) {
        initRecycleProduct(response);
    }

    @Override
    public void failure(String message) {

    }

    @Override
    public void showMessage(String message) {
        getMvpPresenter().getListWishlist(vinesSession.getToken(),0,10,vinesSession.getLoginResponse().getData().getUser_data().get(0).getUser_id());
        Toast.makeText(WishListActivity.this,"Unlike item success",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBuyClicked(WishListResponse.Data item) {
        List<ProductDetailResponse.Data> newCart = new ArrayList<>();
        ProductDetailResponse.Data ne = new ProductDetailResponse.Data(
                item.getDatecreate(),item.getCategory_name(),item.getImage(),item.getSub_region(),Integer.parseInt(item.getAbv()),null,item.getDiscount(),0,item.getPrice()
                ,item.getSummary(),item.getCode(),item.getName(),item.getCategory_id(),item.getProduct_id());
        if (vinesSession.getCart() != null) {
            newCart = vinesSession.getCart();
        }
        newCart.add(ne);
        vinesSession.setCart(newCart);
        Intent intentToCart = new Intent(WishListActivity.this, MyCartActivity.class);
        startActivity(intentToCart);
    }
}
