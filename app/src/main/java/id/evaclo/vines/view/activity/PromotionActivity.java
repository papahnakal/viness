package id.evaclo.vines.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.evaclo.vines.R;
import id.evaclo.vines.view.adapter.PromotionAdapter;
import id.evaclo.vines.view.adapter.StoreAdapter;
import id.evaclo.vines_service.core.BaseActivity;
import id.evaclo.vines_service.model.promotion.PromotionListProductResponse;
import id.evaclo.vines_service.model.promotion.PromotionListResponse;
import id.evaclo.vines_service.model.store.StoreListResponse;
import id.evaclo.vines_service.presenter.PromotionPresenter;
import id.evaclo.vines_service.service.VinesSession;
import id.evaclo.vines_service.view.PromotionView;

public class PromotionActivity extends BaseActivity<PromotionView,PromotionPresenter> implements PromotionView,PromotionAdapter.OnItemClickListener {

    @BindView(R.id.text_toolbar_title)
    TextView textToolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.image_store)
    ImageView imageStore;
    @BindView(R.id.rel_banner)
    RelativeLayout relBanner;
    @BindView(R.id.text_promo)
    TextView textPromo;
    @BindView(R.id.recycler_promo)
    RecyclerView recyclerPromo;

    VinesSession vinesSession;
    List<PromotionListResponse.Data> data;
    LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promotion);
        ButterKnife.bind(this);
        getMvpPresenter().getListPromotion(10);
        setToolbar("PROMOTIONS");
    }

    public void setToolbar(String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        textToolbarTitle.setText(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.icons_navigation_arrow_left);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return true;
    }

    @Override
    public PromotionPresenter createPresenter() {
        vinesSession = new VinesSession(this);
        return new PromotionPresenter(this,vinesSession);
    }

    public void initrecycleview(PromotionListResponse response){
        data = response.getData();
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerPromo.setHasFixedSize(true);
        recyclerPromo.setNestedScrollingEnabled(false);
        recyclerPromo.setLayoutManager(linearLayoutManager);
        recyclerPromo.setAdapter(new PromotionAdapter(false,this,data,this));
    }

    @Override
    public void failure(String message) {

    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void onSuccessGetListPromotion(PromotionListResponse response) {
        initrecycleview(response);
    }

    @Override
    public void onSuccessGetDetailPromotion(PromotionListProductResponse response) {

    }

    @Override
    public void onItemClicked(PromotionListResponse.Data item) {
        Intent toDetailPromo = new Intent(PromotionActivity.this,DetailPromoActivity.class);
        toDetailPromo.putExtra("promotion_id",item.getPromotion_id());
        toDetailPromo.putExtra("banner",item.getImage());
        startActivity(toDetailPromo);
    }
}
