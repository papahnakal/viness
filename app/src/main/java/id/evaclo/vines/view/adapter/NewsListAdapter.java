package id.evaclo.vines.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.evaclo.vines.R;
import id.evaclo.vines.utils.TextFormatter;
import id.evaclo.vines_service.model.news.NewsListResponse;

public class NewsListAdapter extends RecyclerView.Adapter<NewsListAdapter.ViewHolder> {
    private Context context;
    private List<NewsListResponse.Data> data;
    private OnItemClickListener listener;

    public NewsListAdapter(Context context, List<NewsListResponse.Data> data, OnItemClickListener listener) {
        this.context = context;
        this.data = data;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.holder_news, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final NewsListResponse.Data item = data.get(position);
        holder.titleNewsHolder.setText(TextFormatter.make(context).fromHtml(item.getTitle()));
        Picasso.get().load(item.getImage()).fit().into(holder.imageNews);
        holder.linReadmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClicked(item);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image_news)
        ImageView imageNews;
        @BindView(R.id.title_news_holder)
        TextView titleNewsHolder;
        @BindView(R.id.lin_readmore)
        LinearLayout linReadmore;
        @BindView(R.id.card_holder_news)
        CardView cardHolderNews;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnItemClickListener {
        void onItemClicked(NewsListResponse.Data item);
    }
}
