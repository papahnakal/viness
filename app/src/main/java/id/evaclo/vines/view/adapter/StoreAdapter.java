package id.evaclo.vines.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.evaclo.vines.R;
import id.evaclo.vines_service.model.store.StoreListResponse;

public class StoreAdapter extends RecyclerView.Adapter<StoreAdapter.ViewHolder> {
    Context context;
    OnStoreClickedListener listener;
    List<StoreListResponse.Data> data;

    public StoreAdapter(List<StoreListResponse.Data> data,Context context, OnStoreClickedListener listener) {
        this.context = context;
        this.listener = listener;
        this.data = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.holder_all_store, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final StoreListResponse.Data item = data.get(position);
        holder.textOpeningHours.setText(item.getOpen());
        holder.textStoreName.setText(item.getName());
        holder.textStoreAddress.setText(item.getAddress());
        Picasso.get().load(item.getImage()).into(holder.imageStore);
        holder.cardStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onStoreClicked(item);
            }
        });
        holder.imageCallStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onPhoneClicked(item);
            }
        });
        Log.d("imagestore","image store: "+item.getImage());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image_store)
        ImageView imageStore;
        @BindView(R.id.image_call_store)
        ImageView imageCallStore;
        @BindView(R.id.text_store_name)
        TextView textStoreName;
        @BindView(R.id.text_store_address)
        TextView textStoreAddress;
        @BindView(R.id.text_opening_hours)
        TextView textOpeningHours;
        @BindView(R.id.card_store)
        CardView cardStore;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnStoreClickedListener {
        void onStoreClicked(StoreListResponse.Data item);
        void onPhoneClicked(StoreListResponse.Data item);
    }

}
