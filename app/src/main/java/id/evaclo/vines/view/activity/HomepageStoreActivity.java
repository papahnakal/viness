package id.evaclo.vines.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.evaclo.vines.R;
import id.evaclo.vines.view.adapter.FeaturedAdapter;
import id.evaclo.vines.view.adapter.SpecialOfferAdapter;
import id.evaclo.vines_service.core.BaseActivity;
import id.evaclo.vines_service.model.product.ProductDetailResponse;
import id.evaclo.vines_service.model.product.ProductFavoriteListResponse;
import id.evaclo.vines_service.model.product.ProductListResponse;
import id.evaclo.vines_service.model.product.ProductSimilarListResponse;
import id.evaclo.vines_service.model.store.StoreListResponse;
import id.evaclo.vines_service.presenter.ProductPresenter;
import id.evaclo.vines_service.service.VinesSession;
import id.evaclo.vines_service.view.ProductView;

public class HomepageStoreActivity extends BaseActivity<ProductView, ProductPresenter> implements ProductView, FeaturedAdapter.OnItemClickListener, SpecialOfferAdapter.OnItemClickListener {
    LinearLayoutManager linearLayoutManager;
    GridLayoutManager gridLayoutManager;
    @BindView(R.id.text_toolbar_title)
    TextView textToolbarTitle;
    @BindView(R.id.toolbar_store)
    Toolbar toolbarStore;
    @BindView(R.id.rel_search)
    RelativeLayout relSearch;
    @BindView(R.id.image_store)
    ImageView imageStore;
    @BindView(R.id.rel_banner)
    RelativeLayout relBanner;
    @BindView(R.id.text_featured_prod)
    TextView textFeaturedProd;
    @BindView(R.id.recycler_featured_prod)
    RecyclerView recyclerFeaturedProd;
    @BindView(R.id.recycler_special_offers)
    RecyclerView recyclerSpecialOffers;
    @BindView(R.id.text_product)
    TextView textProduct;
    @BindView(R.id.image_filter)
    ImageView imageFilter;
    @BindView(R.id.text_filter)
    TextView textFilter;
    @BindView(R.id.recycler_product)
    RecyclerView recyclerProduct;

    VinesSession vinesSession;
    List<ProductDetailResponse.Data> dataCart = new ArrayList<>();
    List<ProductListResponse.Data> data = new ArrayList<>();
    List<ProductFavoriteListResponse.Data> dataFavorite = new ArrayList<>();
    StoreListResponse.Data store;
    String imagePath, storeName;
    @BindView(R.id.edit_search)
    EditText editSearch;
    @BindView(R.id.image_search)
    ImageView imageSearch;
    @BindView(R.id.rel_img_search)
    RelativeLayout relImgSearch;
    @BindView(R.id.fab)
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage_store);
        ButterKnife.bind(this);
        store = getIntent().getExtras().getParcelable("intent");
        imagePath = store.getImage();
        storeName = store.getName();
        Picasso.get().load(imagePath).fit().centerCrop().into(imageStore);
        //Log.d("id",storeId);
        setToolbar(storeName);
        //initRecycleFeatureProduct();
        //initRecycleSpecialOffers();
        //initRecycleProduct();
        Log.d("STore", storeName + " store id: " + store.getStore_id());
        //getMvpPresenter().getListProduct("", store.getStore_id(), 0, 10);
        //getMvpPresenter().getListProduct(editSearch.getText().toString(), store.getStore_id(), 0, 10);
        getMvpPresenter().getFavoriteProduct(10);
        editSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH ||
                        actionId == EditorInfo.IME_ACTION_DONE ||
                        event != null &&
                                event.getAction() == KeyEvent.ACTION_DOWN &&
                                event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    if (event == null || !event.isShiftPressed()) {
                        getMvpPresenter().getListProduct(editSearch.getText().toString(),"","", store.getStore_id(), 1, 10,"");
                        return true;
                    }
                }
                return false;
            }
        });
        onSearchClicked();
    }

    public void setToolbar(String storeName) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_store);
        textToolbarTitle.setText(storeName);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.icons_navigation_arrow_left);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                vinesSession.clearCart();
        }
        return true;
    }

    @Override
    public void onBack() {
        super.onBack();
        vinesSession.clearCart();
    }

    public void initFavoriteProduct(ProductFavoriteListResponse response) {
        dataFavorite.clear();
        dataFavorite = response.getData();
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerFeaturedProd.setHasFixedSize(true);
        recyclerFeaturedProd.setNestedScrollingEnabled(false);
        recyclerFeaturedProd.setLayoutManager(linearLayoutManager);
        recyclerFeaturedProd.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        recyclerFeaturedProd.setAdapter(new FeaturedAdapter(dataFavorite, this, this));
    }
    //not used
    /*public void initRecycleSpecialOffers() {
        gridLayoutManager = new GridLayoutManager(this, 2);
        recyclerSpecialOffers.setHasFixedSize(true);
        recyclerSpecialOffers.setLayoutManager(gridLayoutManager);
        recyclerSpecialOffers.setAdapter(new SpecialOfferAdapter(this, this));

    }*/

    public void initRecycleProduct(ProductListResponse response) {
        if(data.size()>0) {
            data.clear();
        }
        data = response.getData();
        gridLayoutManager = new GridLayoutManager(this, 2);
        recyclerProduct.setLayoutManager(gridLayoutManager);
        recyclerProduct.setAdapter(new SpecialOfferAdapter(data, this, this));
    }

    @Override
    public void onFeaturedItemClicked(ProductFavoriteListResponse.Data item) {
        Intent a = new Intent(HomepageStoreActivity.this, DetailProduct.class);
        a.putExtra("productid", item.getProduct_id());
        a.putExtra("storeId", store.getStoreId());
        a.putExtra("store_id", store.getStore_id());
        startActivity(a);
    }

    @Override
    public void onSpecialOfferItemClicked(ProductListResponse.Data item) {
        Intent a = new Intent(HomepageStoreActivity.this, DetailProduct.class);
        a.putExtra("productid", item.getProduct_id());
        a.putExtra("storeId", store.getStoreId());
        a.putExtra("store_id", store.getStore_id());
        startActivity(a);
    }

    @Override
    public void onAddToCartClicked(ProductListResponse.Data item) {
        getMvpPresenter().getDetailProduct(store.getStoreId(), item.getProduct_id());

    }

    @Override
    public ProductPresenter createPresenter() {
        vinesSession = new VinesSession(this);
        return new ProductPresenter(this, vinesSession);
    }

    @Override
    public void failure(String message) {

    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void onSuccessGetListProduct(ProductListResponse response) {
        initRecycleProduct(response);
    }

    @Override
    public void onSuccessGetSimilarProduct(ProductSimilarListResponse response) {

    }

    @Override
    public void onSuccessGetFavoriteProduct(ProductFavoriteListResponse response) {
        initFavoriteProduct(response);
    }

    @Override
    public void onSuccessGetDetailProduct(ProductDetailResponse response) {
        dataCart = response.getData();
        List<ProductDetailResponse.Data> newCart = new ArrayList<>();

        if (vinesSession.getCart() != null) {
            newCart = vinesSession.getCart();
        }
        newCart.add(dataCart.get(0));
        vinesSession.setCart(newCart);
        Toast.makeText(HomepageStoreActivity.this, "Product has been add to Cart", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccessAddWishlist(String message) {

    }

    @OnClick(R.id.image_search)
    public void onSearchClicked() {
        getMvpPresenter().getListProduct(editSearch.getText().toString(),"","", store.getStore_id(), 1, 10,"");
        hideSoftkeybard(imageSearch);
    }

    private void hideSoftkeybard(View v) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    @OnClick(R.id.rel_img_search)
    public void onRelSearchClicked() {
        getMvpPresenter().getListProduct(editSearch.getText().toString(),"","",store.getStore_id(), 1, 10,"");
        hideSoftkeybard(imageSearch);
    }

    @OnClick(R.id.fab)
    public void onFabClicked() {
        Intent intentToCart = new Intent(HomepageStoreActivity.this, MyCartActivity.class);
        startActivity(intentToCart);

    }
}
