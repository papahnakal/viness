package id.evaclo.vines.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.evaclo.vines.R;
import id.evaclo.vines_service.model.product.ProductFavoriteListResponse;

public class FeaturedAdapter extends RecyclerView.Adapter<FeaturedAdapter.ViewHolder> {
    Context context;
    OnItemClickListener listener;
    List<ProductFavoriteListResponse.Data> dataFavorite;

    public FeaturedAdapter(List<ProductFavoriteListResponse.Data> dataFavorite, Context context, OnItemClickListener listener) {
        this.context = context;
        this.listener = listener;
        this.dataFavorite = dataFavorite;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.holder_featured_prod, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final ProductFavoriteListResponse.Data item = dataFavorite.get(position);
        holder.textProductName.setText(item.getName());
        holder.textProductType.setText(item.getCategory_name());
        Picasso.get().load(item.getImage()).into(holder.imageProduct);
        holder.cardProd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onFeaturedItemClicked(item);
            }
        });
        if(item.getDiscount()!=0){
            holder.textDiscount.setText(item.getDiscount()+"%");
        }else {
            holder.linDiscount.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return dataFavorite.size();
    }


    static

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image_product)
        ImageView imageProduct;
        @BindView(R.id.text_product_type)
        TextView textProductType;
        @BindView(R.id.text_product_name)
        TextView textProductName;
        @BindView(R.id.card_prod)
        CardView cardProd;
        @BindView(R.id.text_discount)
        TextView textDiscount;
        @BindView(R.id.lin_discount)
        LinearLayout linDiscount;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnItemClickListener {
        void onFeaturedItemClicked(ProductFavoriteListResponse.Data item);
    }
}
