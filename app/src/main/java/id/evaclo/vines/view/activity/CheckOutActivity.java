package id.evaclo.vines.view.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import com.midtrans.sdk.corekit.callback.TransactionFinishedCallback;
import com.midtrans.sdk.corekit.core.LocalDataHandler;
import com.midtrans.sdk.corekit.core.MidtransSDK;
import com.midtrans.sdk.corekit.core.PaymentMethod;
import com.midtrans.sdk.corekit.core.TransactionRequest;
import com.midtrans.sdk.corekit.models.BillingAddress;
import com.midtrans.sdk.corekit.models.CustomerDetails;
import com.midtrans.sdk.corekit.models.ItemDetails;
import com.midtrans.sdk.corekit.models.ShippingAddress;
import com.midtrans.sdk.corekit.models.UserAddress;
import com.midtrans.sdk.corekit.models.UserDetail;
import com.midtrans.sdk.corekit.models.snap.CreditCard;
import com.midtrans.sdk.corekit.models.snap.TransactionResult;
import com.midtrans.sdk.uikit.SdkUIFlowBuilder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.evaclo.vines.BuildConfig;
import id.evaclo.vines.R;
import id.evaclo.vines.utils.TextFormatter;
import id.evaclo.vines.view.adapter.CheckoutAdapter;
import id.evaclo.vines_service.core.BaseActivity;
import id.evaclo.vines_service.model.order.GetOrderCodeResponse;
import id.evaclo.vines_service.model.product.ProductDetailResponse;
import id.evaclo.vines_service.presenter.CheckoutPresenter;
import id.evaclo.vines_service.service.VinesSession;
import id.evaclo.vines_service.view.CheckoutView;

public class CheckOutActivity extends BaseActivity<CheckoutView, CheckoutPresenter> implements CheckoutView,TransactionFinishedCallback {

    @BindView(R.id.text_toolbar_title)
    TextView textToolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recycler_summary)
    RecyclerView recyclerSummary;
    @BindView(R.id.text_subtotal)
    TextView textSubtotal;
    @BindView(R.id.button_paynow)
    Button buttonPaynow;
    LinearLayoutManager linearLayoutManager;
    ArrayList<ProductDetailResponse.Data> datas = new ArrayList<>();
    String amount;
    VinesSession session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out);
        ButterKnife.bind(this);
        datas = getIntent().getParcelableArrayListExtra("data");
        amount = getIntent().getStringExtra("amount");
        textSubtotal.setText(TextFormatter.make(this).IDRFormat(amount + ""));
        initrecycleview(datas);
        setToolbar();
        initMidtrans();
    }

    public void initrecycleview(List<ProductDetailResponse.Data> data) {
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerSummary.setHasFixedSize(true);
        recyclerSummary.setNestedScrollingEnabled(false);
        recyclerSummary.setLayoutManager(linearLayoutManager);
        recyclerSummary.setAdapter(new CheckoutAdapter(this, data));
    }

    public void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        textToolbarTitle.setText("CHECK OUT");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.icons_navigation_arrow_left);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return true;
    }


    @Override
    public void onFailure(String message) {

    }

    @Override
    public void onSuccessGetOrderId(GetOrderCodeResponse response) {
        initSetPayment(response.getData().get(0).getOrder_code(),Double.parseDouble(amount));
    }

    @Override
    public CheckoutPresenter createPresenter() {
        session = new VinesSession(this);
        return new CheckoutPresenter(this, session);
    }

    @OnClick(R.id.button_paynow)
    public void onViewClicked() {
        getMvpPresenter().getOrderId(session.getLoginResponse().getData().getUser_data().get(0).getUser_id());
    }

    private CustomerDetails initCustomerDetails(ShippingAddress shipping,BillingAddress bil_address) {

        //define customer detail (mandatory for coreflow)
        CustomerDetails mCustomerDetails = new CustomerDetails();
        mCustomerDetails.setPhone(session.getLoginResponse().getData().getUser_data().get(0).getPhone());
        mCustomerDetails.setFirstName(session.getLoginResponse().getData().getUser_data().get(0).getFullname());
        mCustomerDetails.setEmail(session.getLoginResponse().getData().getUser_data().get(0).getEmail());
        mCustomerDetails.setBillingAddress(bil_address);
        mCustomerDetails.setShippingAddress(shipping);

        return mCustomerDetails;
    }


    public void initSetPayment(String orderId, Double amount){
        TransactionRequest transactionRequest = new TransactionRequest(orderId, amount);

        BillingAddress bil_address = new BillingAddress(
                session.getLoginResponse().getData().getUser_data().get(0).getFullname(),
                "",
                "Jakarta",
                "Jakarta",
                "",
                session.getLoginResponse().getData().getUser_data().get(0).getPhone(),
                "IDN"
        );

        ShippingAddress shipping = new ShippingAddress();
        shipping.setAddress("Jakarta");
        shipping.setCity("jakarta");
        shipping.setCountryCode("");
        shipping.setFirstName(session.getLoginResponse().getData().getUser_data().get(0).getFullname());
        shipping.setLastName("");
        shipping.setPhone(session.getLoginResponse().getData().getUser_data().get(0).getPhone());
        shipping.setCountryCode("IDN");

        transactionRequest.setCustomerDetails(initCustomerDetails(shipping,bil_address));

        UserAddress userAddress = new UserAddress();
        userAddress.setAddress("");
        userAddress.setCity("Jakarta");
        userAddress.setZipcode("");
        userAddress.setCountry("IDN");

        ArrayList<UserAddress> userAddresses = new ArrayList<>();
        userAddresses.add(userAddress);
        UserDetail userDetail = new UserDetail();
        userDetail.setUserFullName(session.getLoginResponse().getData().getUser_data().get(0).getFullname());
        userDetail.setEmail(session.getLoginResponse().getData().getUser_data().get(0).getEmail());
        userDetail.setPhoneNumber(session.getLoginResponse().getData().getUser_data().get(0).getPhone());
        userDetail.setUserAddresses(userAddresses);
        LocalDataHandler.saveObject("user_details", userDetail);


// Create array list and add above item details in it and then set it to transaction request.
        ArrayList<ItemDetails> itemDetailsList = new ArrayList<>();
        for (int i = 0; i < datas.size(); i++) {
            itemDetailsList.add(new ItemDetails(datas.get(i).getCode(),datas.get(i).getPrice(),1,datas.get(i).getName()));
        }
        //itemDetailsList.add(itemDetails1);
        //itemDetailsList.add(itemdetails2);
        CreditCard creditCardOptions = new CreditCard();
        creditCardOptions.setSaveCard(false);
        creditCardOptions.setSecure(false);

        transactionRequest.setCreditCard(creditCardOptions);
        transactionRequest.setCardPaymentInfo(getString(R.string.card_click_type_none), true);

// Set item details into the transaction request.
        transactionRequest.setItemDetails(itemDetailsList);
        MidtransSDK.getInstance().setTransactionRequest(transactionRequest);
        MidtransSDK.getInstance().startPaymentUiFlow(CheckOutActivity.this, PaymentMethod.BCA_KLIKPAY);
    }
    public void initMidtrans(){
        SdkUIFlowBuilder.init()
                .setClientKey(BuildConfig.CLIENT_KEY) // client_key is mandatory
                .setContext(getApplicationContext()) // context is mandatory
                .setTransactionFinishedCallback(new TransactionFinishedCallback() {
                    @Override
                    public void onTransactionFinished(TransactionResult result) {
                        // Handle finished transaction here.
                    }
                }) // set transaction finish callback (sdk callback)
                .setMerchantBaseUrl(BuildConfig.BASE_URL_MERCHANT) //set merchant url (required)
                .enableLog(true) // enable sdk log (optional)
                //.setColorTheme(new CustomColorTheme("#FFE51255", "#B61548", "#FFE51255")) // set theme. it will replace theme on snap theme on MAP ( optional)
                .buildSDK();
    }

    @Override
    public void onTransactionFinished(TransactionResult transactionResult) {

    }
}
