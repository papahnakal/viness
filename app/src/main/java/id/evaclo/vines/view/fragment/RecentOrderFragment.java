package id.evaclo.vines.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import id.evaclo.vines.R;
import id.evaclo.vines.view.adapter.OrderAdapter;
import id.evaclo.vines.view.adapter.RecentOrderAdapter;
import id.evaclo.vines_service.core.BaseFragment;
import id.evaclo.vines_service.model.order.HistoryOrderResponse;
import id.evaclo.vines_service.model.order.RecentOrderResponse;
import id.evaclo.vines_service.presenter.OrderPresenter;
import id.evaclo.vines_service.service.VinesSession;
import id.evaclo.vines_service.view.OrderView;


public class RecentOrderFragment extends BaseFragment<OrderView, OrderPresenter> implements OrderView,RecentOrderAdapter.OnItemClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.recycler_recent_order)
    RecyclerView recyclerRecentOrder;
    Unbinder unbinder;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    VinesSession vinesSession;
    List<RecentOrderResponse.Data> data = new ArrayList<>();
    LinearLayoutManager linearLayoutManager;

    public RecentOrderFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment RecentOrderFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static RecentOrderFragment newInstance(String param1, String param2) {
        RecentOrderFragment fragment = new RecentOrderFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_recent_order, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getMvpPresenter().getRecentOrder(vinesSession.getToken(),0,10,vinesSession.getLoginResponse().getData().getUser_data().get(0).getUser_id());
        //getMvpPresenter().getRecentOrder("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Iml3YW5faW5mb3JAeWFob28uY29tIiwidXNlcl9pZCI6MjEsImV4cCI6MTU0NDM4Njk1Njg1NCwiaWF0IjoxNTQzNzgyMTU2fQ.PswZebEFmjxxBaKYL5tjLEo8djHDmvhVBXxHVM4FWA8",0,10,21);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    public void initrecycleview(RecentOrderResponse response){
        data = response.getData();
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerRecentOrder.setHasFixedSize(true);
        recyclerRecentOrder.setNestedScrollingEnabled(false);
        recyclerRecentOrder.setLayoutManager(linearLayoutManager);
        recyclerRecentOrder.setAdapter(new RecentOrderAdapter(data,getActivity(),this));
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public OrderPresenter createPresenter() {
        vinesSession = new VinesSession(getActivity());
        return new OrderPresenter(getActivity(), vinesSession);
    }

    @Override
    public void failure(String message) {

    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void onSuccessGetHistoryOrder(HistoryOrderResponse response) {

    }

    @Override
    public void onSuccessGetRecentOrder(RecentOrderResponse response) {
        initrecycleview(response);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onItemClicked(RecentOrderResponse.Data data) {

    }
}
