package id.evaclo.vines.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.evaclo.vines.R;
import id.evaclo.vines.utils.TextFormatter;
import id.evaclo.vines_service.model.product.ProductDetailResponse;
import id.evaclo.vines_service.model.product.ProductListResponse;

public class SpecialOfferAdapter extends RecyclerView.Adapter<SpecialOfferAdapter.ViewHolder> {
    Context context;
    OnItemClickListener listener;
    List<ProductListResponse.Data> data;

    public SpecialOfferAdapter(List<ProductListResponse.Data> data, Context context, OnItemClickListener listener) {
        this.context = context;
        this.listener = listener;
        this.data = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.holder_special_offer, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final ProductListResponse.Data item = data.get(position);
        Log.d("image","image : "+item.getImage());
        if(item.getPrice()!=0) {
            holder.textPrice.setText(TextFormatter.make(context).IDRFormat(item.getPrice() + ""));
        }else{
            holder.textPrice.setText("Product not available in this store");
        }
        holder.textPriceStrikeout.setText("");
        holder.textProductName.setText(item.getName());
        holder.textProductType.setText(item.getCategory_name());
        Picasso.get().load(item.getImage()).fit().into(holder.imageProduct);
        holder.cardProd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onSpecialOfferItemClicked(item);
            }
        });
        if(item.getDiscount()>0){
            holder.linDiscount.setVisibility(View.VISIBLE);
            holder.textDiscount.setText(item.getDiscount()+"%");
        }else {
            holder.linDiscount.setVisibility(View.GONE);
        }
        if(item.getIs_favorite()){
            holder.image_like.setImageResource(R.drawable.ico_nav_favorite_full);
        }else {
            holder.image_like.setImageResource(R.drawable.ico_nav_favorite);
        }
        holder.lin_add_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onAddToCartClicked(item);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image_product)
        ImageView imageProduct;
        @BindView(R.id.text_product_type)
        TextView textProductType;
        @BindView(R.id.text_product_name)
        TextView textProductName;
        @BindView(R.id.text_price_strikeout)
        TextView textPriceStrikeout;
        @BindView(R.id.text_price)
        TextView textPrice;
        @BindView(R.id.card_prod)
        CardView cardProd;
        @BindView(R.id.text_discount)
        TextView textDiscount;
        @BindView(R.id.lin_discount)
        LinearLayout linDiscount;
        @BindView(R.id.image_like)
        ImageView image_like;
        @BindView(R.id.lin_add_cart)
        LinearLayout lin_add_cart;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnItemClickListener {
        void onSpecialOfferItemClicked(ProductListResponse.Data item );
        void onAddToCartClicked(ProductListResponse.Data item );
    }
}
