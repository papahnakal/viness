package id.evaclo.vines.view.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.evaclo.vines.R;
import id.evaclo.vines_service.core.BaseActivity;
import id.evaclo.vines_service.model.register.RegisterResponse;
import id.evaclo.vines_service.presenter.SignUpPresenter;
import id.evaclo.vines_service.service.VinesSession;
import id.evaclo.vines_service.view.SignUpView;

public class SignUpActivity extends BaseActivity<SignUpView, SignUpPresenter> implements SignUpView, DatePickerDialog.OnDateSetListener {
    private static final int SELECT_IMAGE = 99;
    @BindView(R.id.toolabar)
    Toolbar toolabar;
    @BindView(R.id.text_dosnot)
    TextView textDosnot;
    @BindView(R.id.text_sign_in)
    TextView textSignIn;
    @BindView(R.id.button_sign_up)
    Button buttonSignUp;
    @BindView(R.id.lin_upload_picture)
    LinearLayout linUploadPicture;
    @BindView(R.id.edit_first_name)
    EditText editFirstName;
    @BindView(R.id.edit_last_name)
    EditText editLastName;
    @BindView(R.id.edit_email)
    EditText editEmail;
    @BindView(R.id.edit_password)
    EditText editPassword;
    @BindView(R.id.edit_phone)
    EditText editPhone;

    VinesSession vinesSession;
    @BindView(R.id.edit_date)
    EditText editDate;
    @BindView(R.id.edit_month)
    EditText editMonth;
    @BindView(R.id.edit_years)
    EditText editYears;
    @BindView(R.id.image_profile)
    ImageView imageProfile;
    private int mYear, mMonth, mDay;
    Bitmap bitmap;
    String date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.text_sign_in, R.id.button_sign_up, R.id.lin_upload_picture})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.text_sign_in:
                break;
            case R.id.button_sign_up:
                getMvpPresenter().postRegister(editFirstName.getText().toString() + " " + editLastName.getText().toString()
                        , editEmail.getText().toString()
                        , editPassword.getText().toString()
                        , editPhone.getText().toString()
                        , date
                        , bitmap);
                break;
            case R.id.lin_upload_picture:
                openGalery();
                break;
        }
    }

    public void openGalery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_IMAGE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), data.getData());
                        imageProfile.setImageBitmap(bitmap);
                        //Uri src = getImageUri(getApplicationContext(), bitmap);
                        //images = new File(getRealPathFromURI(src));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(SignUpActivity.this, "Canceled", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public SignUpPresenter createPresenter() {
        vinesSession = new VinesSession(this);
        return new SignUpPresenter(this, vinesSession);
    }

    @Override
    public void onSuccess(RegisterResponse response) {
        if (response.getStatus() == 200) {
            finish();
        }
    }

    @Override
    public void onFailed(String message) {

    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(SignUpActivity.this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @OnClick({R.id.edit_date, R.id.edit_month, R.id.edit_years})
    public void onDateClicked(View view) {
        switch (view.getId()) {
            case R.id.edit_date:
                showDatePicker();
                break;
            case R.id.edit_month:
                showDatePicker();
                break;
            case R.id.edit_years:
                showDatePicker();
                break;
        }
    }

    @OnClick(R.id.image_profile)
    public void onViewClicked() {
        openGalery();
    }
    private void showDatePicker() {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(SignUpActivity.this, R.style.DialogTheme,
                this, mYear, mMonth, mDay);

        datePickerDialog.show();

    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        String month = String.valueOf(monthOfYear);
        String day = String.valueOf(dayOfMonth);
        day = String.valueOf(dayOfMonth);
        if (dayOfMonth < 10) {
            day = String.valueOf("0" + dayOfMonth);
        }
        month = String.valueOf(monthOfYear + 1);
        if (monthOfYear < 9) {
            month = String.valueOf("0" + (monthOfYear + 1));
        }

        date = year + "-" + month + "-" + day;
        editDate.setText(day);
        editMonth.setText(month);
        editYears.setText(year+"");
    }
}
