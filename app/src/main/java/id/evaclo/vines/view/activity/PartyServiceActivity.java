package id.evaclo.vines.view.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.evaclo.vines.R;
import id.evaclo.vines_service.core.BaseActivity;
import id.evaclo.vines_service.model.PartyResponse;
import id.evaclo.vines_service.presenter.PartyPresenter;
import id.evaclo.vines_service.service.VinesSession;
import id.evaclo.vines_service.view.PartyView;

public class PartyServiceActivity extends BaseActivity<PartyView,PartyPresenter>implements PartyView {

    @BindView(R.id.text_toolbar_title)
    TextView textToolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.image_store)
    ImageView imageStore;
    @BindView(R.id.rel_banner)
    RelativeLayout relBanner;
    @BindView(R.id.edit_first_name)
    EditText editFirstName;
    @BindView(R.id.edit_lst_name)
    EditText editLstName;
    @BindView(R.id.edit_email_address)
    EditText editEmailAddress;
    @BindView(R.id.edit_phone_number)
    EditText editPhoneNumber;
    @BindView(R.id.edit_party_theme)
    EditText editPartyTheme;
    @BindView(R.id.edit_locations)
    EditText editLocations;
    @BindView(R.id.edit_budget)
    EditText editBudget;
    @BindView(R.id.edit_attendess)
    EditText editAttendess;
    @BindView(R.id.edit_date)
    EditText editDate;
    @BindView(R.id.radioWine)
    CheckBox radioWine;
    @BindView(R.id.radioBeer)
    CheckBox radioBeer;
    @BindView(R.id.radioSpirits)
    CheckBox radioSpirits;
    /*@BindView(R.id.radioLiquor)
    RadioGroup radioLiquor;*/
    @BindView(R.id.linear_form)
    LinearLayout linearForm;
    @BindView(R.id.button_solved)
    Button buttonSolved;
    VinesSession vinesSession;
    String category;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_party_service);
        ButterKnife.bind(this);
        setToolbar();
    }

    public void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        textToolbarTitle.setText("PARTY SERVICE");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.icons_navigation_arrow_left);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return true;
    }

    @OnClick(R.id.button_solved)
    public void onViewClicked() {
        //category();
        getMvpPresenter().postParty(editFirstName.getText().toString(),
                editLstName.getText().toString(),
                editEmailAddress.getText().toString(),
                editPhoneNumber.getText().toString(),
                editLocations.getText().toString(),
                editBudget.getText().toString(),
                editAttendess.getText().toString(),editDate.getText().toString(),category());
    }

    private String category(){
        String category = "";
        //int select = radioLiquor.getCheckedRadioButtonId();
        if(radioBeer.isChecked()){
            category= category+"Beer,";
        }
        if(radioWine.isChecked()){
            category = category+"Wine,";
        }
        if (radioSpirits.isChecked()){
            category = category+"Liquor,";
        }
        Log.d("category","category : "+category.trim());

        if(category.length()>0)
        category.substring(0,category.length()-1);

        return category;
    }

    @Override
    public PartyPresenter createPresenter() {
        vinesSession = new VinesSession(this);
        return new PartyPresenter(this,vinesSession);
    }

    @Override
    public void failure(String message) {

    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(PartyServiceActivity.this,message,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccessPostParty(PartyResponse response) {
        Toast.makeText(PartyServiceActivity.this,response.getDisplay_message(),Toast.LENGTH_SHORT).show();
    }
}
