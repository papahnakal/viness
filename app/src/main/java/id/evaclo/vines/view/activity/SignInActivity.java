package id.evaclo.vines.view.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.evaclo.vines.R;
import id.evaclo.vines_service.core.BaseActivity;
import id.evaclo.vines_service.model.login.LoginResponse;
import id.evaclo.vines_service.presenter.SignInPresenter;
import id.evaclo.vines_service.service.VinesSession;
import id.evaclo.vines_service.view.SignInView;

public class SignInActivity extends BaseActivity<SignInView,SignInPresenter>implements SignInView {
    private static final int REQUEST_LOCATION = 100;
    @BindView(R.id.text_dosnot)
    TextView textDosnot;
    @BindView(R.id.text_sign_up)
    TextView textSignUp;
    @BindView(R.id.button_sign_in)
    Button buttonSignIn;
    @BindView(R.id.edit_email)
    EditText editEmail;
    @BindView(R.id.edit_password)
    EditText editPassword;
    @BindView(R.id.text_forgot_password)
    TextView textForgotPassword;

    VinesSession vinesSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);
        editEmail.setText("rachmatsatria@gmail.com");
        editPassword.setText("satriaairtas");
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            // Check permission
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_LOCATION);
        }
    }

    @OnClick({R.id.text_sign_up, R.id.button_sign_in, R.id.text_forgot_password})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.text_sign_up:
                Intent toSignUp = new Intent(SignInActivity.this,SignUpActivity.class);
                startActivity(toSignUp);
                break;
            case R.id.button_sign_in:
                getMvpPresenter().getAuthentication(editEmail.getText().toString(),editPassword.getText().toString());
                break;
            case R.id.text_forgot_password:
                Intent toForgot = new Intent(SignInActivity.this,FogetPasswordActivity.class);
                startActivity(toForgot);
                break;
        }
    }

    @Override
    public SignInPresenter createPresenter() {
        vinesSession = new VinesSession(this);
        return new SignInPresenter(this, vinesSession);
    }

    @Override
    public void onSuccess(LoginResponse response) {
        Log.d("LOGIN","token : "+response.getData().getToken());
        Intent toHome = new Intent(SignInActivity.this,HomepageActivity.class);
        startActivity(toHome);
        finish();
    }

    @Override
    public void onFailed(String message) {
        Log.d("LOGIN", "failed : "+message);
    }

    @Override
    public void showMessage(String message) {
        Log.d("LOGIN", "message : "+message);
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }
}
