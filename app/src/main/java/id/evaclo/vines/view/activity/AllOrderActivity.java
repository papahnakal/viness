package id.evaclo.vines.view.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.evaclo.vines.R;
import id.evaclo.vines.view.fragment.HistoryOrderFragment;
import id.evaclo.vines.view.fragment.RecentOrderFragment;

public class AllOrderActivity extends AppCompatActivity {

    @BindView(R.id.text_toolbar_title)
    TextView textToolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tab_my_order)
    TabLayout tabMyOrder;
    @BindView(R.id.pager_my_order)
    ViewPager pagerMyOrder;
    ViewPagerAdapter viewPagerAdapter;
    RecentOrderFragment recentOrderFragment;
    HistoryOrderFragment historyOrderFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_order);
        ButterKnife.bind(this);
        setToolbar("ALL ORDERS");
        pagerMyOrder.setOffscreenPageLimit(1);
        setupViewPager(pagerMyOrder);
        tabMyOrder.setupWithViewPager(pagerMyOrder);
    }
    public void setToolbar(String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        textToolbarTitle.setText(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.icons_navigation_arrow_left);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return true;
    }
    private void setupViewPager(ViewPager viewPager){
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(new RecentOrderFragment(),"Recent Orders");
        viewPagerAdapter.addFragment(new HistoryOrderFragment(),"History Orders");
        viewPager.setAdapter(viewPagerAdapter);
    }
    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();
        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 0:
                    //gAnalyticsClass.analyticClickEvent("R.id.adapter_tab_mycourse","event_click_my_purchased_tab","tab_view");
                    recentOrderFragment = new RecentOrderFragment();
                    return recentOrderFragment;
                case 1:
                    //gAnalyticsClass.analyticClickEvent("R.id.adapter_tab_saved","event_click_my_saved_tab","tab_view");
                    historyOrderFragment = new HistoryOrderFragment();
                    return historyOrderFragment;
            }
            return  null;
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }
        public void addFragment(Fragment fragment,String title){
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
