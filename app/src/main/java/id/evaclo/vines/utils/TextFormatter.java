package id.evaclo.vines.utils;

import android.content.Context;
import android.text.Html;
import android.text.Spanned;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TextFormatter {
    Context context;
    private TextFormatter(Context context) {
        this.context = context;
    }

    public static TextFormatter make(Context context) {
        return new TextFormatter(context);
    }

    public static Spanned fromHtml(String html){
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html,Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }


    public String IDRFormat(String price) {
        if(price!=null) {
            String result = "";
            if (price.equalsIgnoreCase("0")) {
                result = "Rp 0";
                return result;
            } else {
                int lenght = price.length();
                if (lenght <= 3) {
                    result = "Rp " + price;
                } else if ((lenght >= 4) && (lenght <= 6)) {
                    String reverse = new StringBuilder(price).reverse().toString();
                    String tigablkng = reverse.substring(0, 3);
                    String depan = reverse.substring(3, reverse.length());
                    result = "Rp " + new StringBuilder(tigablkng + "." + depan).reverse().toString();
                } else if (lenght >= 7 && lenght <= 9) {
                    String reverse = new StringBuilder(price).reverse().toString();
                    String tigablkng = reverse.substring(0, 3);
                    String tigatengah = reverse.substring(3, 6);
                    String depan = reverse.substring(6, reverse.length());
                    result = "Rp " + new StringBuilder(tigablkng + "." + tigatengah + "." + depan).reverse().toString();
                } else if (lenght >= 10 && lenght <= 12) {
                    String reverse = new StringBuilder(price).reverse().toString();
                    String tigablkng = reverse.substring(0, 3);
                    String tigatengah = reverse.substring(3, 6);
                    String tigatengah2 = reverse.substring(6, 9);
                    String depan = reverse.substring(9, reverse.length());
                    result = "Rp " + new StringBuilder(tigablkng + "." + tigatengah + "." + tigatengah2 + "." + depan).reverse().toString();
                }
                return result;
            }
        }else {
            return "Rp 0";
        }
    }
    public String DateFormat(String in){
        String[] strings = in.split("T");

        DateFormat inputFormatter1 = new SimpleDateFormat("yyyy-MM-dd");
        Date dateIn = null;
        try {
            dateIn = inputFormatter1.parse(strings[0]);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        DateFormat outputFormatter1 = new SimpleDateFormat("dd-MM-yyyy");
        String dateOut = outputFormatter1.format(dateIn); //
        return dateOut;
    }
}
