package id.evaclo.vines.utils;

public class CountCart {
    int count;

    public CountCart(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
