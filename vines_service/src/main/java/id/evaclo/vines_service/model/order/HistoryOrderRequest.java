package id.evaclo.vines_service.model.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HistoryOrderRequest {

    @Expose
    @SerializedName("token")
    private String token;
    @Expose
    @SerializedName("offset")
    private int offset;
    @Expose
    @SerializedName("limit")
    private int limit;
    @Expose
    @SerializedName("user_id")
    private int user_id;

    public HistoryOrderRequest(String token, int offset, int limit, int user_id) {
        this.token = token;
        this.offset = offset;
        this.limit = limit;
        this.user_id = user_id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
}
