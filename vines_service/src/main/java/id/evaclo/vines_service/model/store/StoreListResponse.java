package id.evaclo.vines_service.model.store;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import id.evaclo.vines_service.core.BaseModel;

public class StoreListResponse extends BaseModel implements Parcelable {

    @Expose
    @SerializedName("time")
    private String time;
    @Expose
    @SerializedName("data")
    private List<Data> data;


    protected StoreListResponse(Parcel in) {
        time = in.readString();
    }

    public static final Creator<StoreListResponse> CREATOR = new Creator<StoreListResponse>() {
        @Override
        public StoreListResponse createFromParcel(Parcel in) {
            return new StoreListResponse(in);
        }

        @Override
        public StoreListResponse[] newArray(int size) {
            return new StoreListResponse[size];
        }
    };

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(time);
    }

    public static class Data implements Parcelable{
        @Expose
        @SerializedName("distance")
        private double distance;
        @Expose
        @SerializedName("image")
        private String image;
        @Expose
        @SerializedName("close")
        private String close;
        @Expose
        @SerializedName("open")
        private String open;
        @Expose
        @SerializedName("phone")
        private String phone;
        @Expose
        @SerializedName("longitude")
        private double longitude;
        @Expose
        @SerializedName("latitude")
        private double latitude;
        @Expose
        @SerializedName("address")
        private String address;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("StoreId")
        private String StoreId;
        @Expose
        @SerializedName("store_id")
        private int store_id;

        protected Data(Parcel in) {
            distance = in.readDouble();
            image = in.readString();
            close = in.readString();
            open = in.readString();
            phone = in.readString();
            longitude = in.readDouble();
            latitude = in.readDouble();
            address = in.readString();
            name = in.readString();
            StoreId = in.readString();
            store_id = in.readInt();
        }

        public static final Creator<Data> CREATOR = new Creator<Data>() {
            @Override
            public Data createFromParcel(Parcel in) {
                return new Data(in);
            }

            @Override
            public Data[] newArray(int size) {
                return new Data[size];
            }
        };

        public double getDistance() {
            return distance;
        }

        public void setDistance(double distance) {
            this.distance = distance;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getClose() {
            return close;
        }

        public void setClose(String close) {
            this.close = close;
        }

        public String getOpen() {
            return open;
        }

        public void setOpen(String open) {
            this.open = open;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getStoreId() {
            return StoreId;
        }

        public void setStoreId(String StoreId) {
            this.StoreId = StoreId;
        }

        public int getStore_id() {
            return store_id;
        }

        public void setStore_id(int store_id) {
            this.store_id = store_id;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeDouble(distance);
            dest.writeString(image);
            dest.writeString(close);
            dest.writeString(open);
            dest.writeString(phone);
            dest.writeDouble(longitude);
            dest.writeDouble(latitude);
            dest.writeString(address);
            dest.writeString(name);
            dest.writeString(StoreId);
            dest.writeInt(store_id);
        }
    }
}
