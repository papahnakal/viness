package id.evaclo.vines_service.model.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import id.evaclo.vines_service.core.BaseModel;

public class HistoryOrderResponse extends BaseModel {

    @Expose
    @SerializedName("time")
    private String time;
    @Expose
    @SerializedName("data")
    private List<Data> data;


    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }


    public static class Data {
        @Expose
        @SerializedName("store_address")
        private String store_address;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("promotion_code")
        private String promotion_code;
        @Expose
        @SerializedName("datecreate")
        private String datecreate;
        @Expose
        @SerializedName("view")
        private String view;
        @Expose
        @SerializedName("status_message")
        private String status_message;
        @Expose
        @SerializedName("transaction_id")
        private String transaction_id;
        @Expose
        @SerializedName("status_code")
        private String status_code;
        @Expose
        @SerializedName("payment_status")
        private String payment_status;
        @Expose
        @SerializedName("use_point")
        private int use_point;
        @Expose
        @SerializedName("point")
        private int point;
        @Expose
        @SerializedName("total_order")
        private int total_order;
        @Expose
        @SerializedName("payment_method")
        private String payment_method;
        @Expose
        @SerializedName("order_code")
        private String order_code;
        @Expose
        @SerializedName("store_id")
        private int store_id;
        @Expose
        @SerializedName("longitude")
        private double longitude;
        @Expose
        @SerializedName("latitude")
        private double latitude;
        @Expose
        @SerializedName("address")
        private String address;
        @Expose
        @SerializedName("delivery_address")
        private String delivery_address;
        @Expose
        @SerializedName("user_id")
        private int user_id;
        @Expose
        @SerializedName("order_id")
        private int order_id;

        public String getStore_address() {
            return store_address;
        }

        public void setStore_address(String store_address) {
            this.store_address = store_address;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPromotion_code() {
            return promotion_code;
        }

        public void setPromotion_code(String promotion_code) {
            this.promotion_code = promotion_code;
        }

        public String getDatecreate() {
            return datecreate;
        }

        public void setDatecreate(String datecreate) {
            this.datecreate = datecreate;
        }

        public String getView() {
            return view;
        }

        public void setView(String view) {
            this.view = view;
        }

        public String getStatus_message() {
            return status_message;
        }

        public void setStatus_message(String status_message) {
            this.status_message = status_message;
        }

        public String getTransaction_id() {
            return transaction_id;
        }

        public void setTransaction_id(String transaction_id) {
            this.transaction_id = transaction_id;
        }

        public String getStatus_code() {
            return status_code;
        }

        public void setStatus_code(String status_code) {
            this.status_code = status_code;
        }

        public String getPayment_status() {
            return payment_status;
        }

        public void setPayment_status(String payment_status) {
            this.payment_status = payment_status;
        }

        public int getUse_point() {
            return use_point;
        }

        public void setUse_point(int use_point) {
            this.use_point = use_point;
        }

        public int getPoint() {
            return point;
        }

        public void setPoint(int point) {
            this.point = point;
        }

        public int getTotal_order() {
            return total_order;
        }

        public void setTotal_order(int total_order) {
            this.total_order = total_order;
        }

        public String getPayment_method() {
            return payment_method;
        }

        public void setPayment_method(String payment_method) {
            this.payment_method = payment_method;
        }

        public String getOrder_code() {
            return order_code;
        }

        public void setOrder_code(String order_code) {
            this.order_code = order_code;
        }

        public int getStore_id() {
            return store_id;
        }

        public void setStore_id(int store_id) {
            this.store_id = store_id;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getDelivery_address() {
            return delivery_address;
        }

        public void setDelivery_address(String delivery_address) {
            this.delivery_address = delivery_address;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public int getOrder_id() {
            return order_id;
        }

        public void setOrder_id(int order_id) {
            this.order_id = order_id;
        }
    }
}
