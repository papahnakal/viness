package id.evaclo.vines_service.model.product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import id.evaclo.vines_service.core.BaseModel;

public class ProductFavoriteListResponse extends BaseModel {

    @Expose
    @SerializedName("time")
    private String time;
    @Expose
    @SerializedName("data")
    private List<Data> data;


    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public static class Data {
        @Expose
        @SerializedName("datecreate")
        private String datecreate;
        @Expose
        @SerializedName("category_name")
        private String category_name;
        @Expose
        @SerializedName("image")
        private String image;
        @Expose
        @SerializedName("sub_region")
        private String sub_region;
        @Expose
        @SerializedName("abv")
        private String abv;
        @Expose
        @SerializedName("discount")
        private int discount;
        @Expose
        @SerializedName("price")
        private int price;
        @Expose
        @SerializedName("summary")
        private String summary;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("category_id")
        private int category_id;
        @Expose
        @SerializedName("product_id")
        private int product_id;

        public String getDatecreate() {
            return datecreate;
        }

        public void setDatecreate(String datecreate) {
            this.datecreate = datecreate;
        }

        public String getCategory_name() {
            return category_name;
        }

        public void setCategory_name(String category_name) {
            this.category_name = category_name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getSub_region() {
            return sub_region;
        }

        public void setSub_region(String sub_region) {
            this.sub_region = sub_region;
        }

        public String getAbv() {
            return abv;
        }

        public void setAbv(String abv) {
            this.abv = abv;
        }

        public int getDiscount() {
            return discount;
        }

        public void setDiscount(int discount) {
            this.discount = discount;
        }

        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }

        public String getSummary() {
            return summary;
        }

        public void setSummary(String summary) {
            this.summary = summary;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getCategory_id() {
            return category_id;
        }

        public void setCategory_id(int category_id) {
            this.category_id = category_id;
        }

        public int getProduct_id() {
            return product_id;
        }

        public void setProduct_id(int product_id) {
            this.product_id = product_id;
        }
    }
}
