package id.evaclo.vines_service.model.about;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import id.evaclo.vines_service.core.BaseModel;

public class AboutResponse extends BaseModel {

    @Expose
    @SerializedName("time")
    private String time;
    @Expose
    @SerializedName("data")
    private List<Data> data;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }


    public static class Data {
        @Expose
        @SerializedName("datecreate")
        private String datecreate;
        @Expose
        @SerializedName("description")
        private String description;
        @Expose
        @SerializedName("summary")
        private String summary;
        @Expose
        @SerializedName("title")
        private String title;
        @Expose
        @SerializedName("page")
        private String page;
        @Expose
        @SerializedName("static_page_id")
        private int static_page_id;

        public String getDatecreate() {
            return datecreate;
        }

        public void setDatecreate(String datecreate) {
            this.datecreate = datecreate;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getSummary() {
            return summary;
        }

        public void setSummary(String summary) {
            this.summary = summary;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getPage() {
            return page;
        }

        public void setPage(String page) {
            this.page = page;
        }

        public int getStatic_page_id() {
            return static_page_id;
        }

        public void setStatic_page_id(int static_page_id) {
            this.static_page_id = static_page_id;
        }
    }
}
