package id.evaclo.vines_service.model.promotion;

public class PromotionDetailRequest {
    	private int promotion_id;
                private int limit;
                private int offset;

    public int getPromotion_id() {
        return promotion_id;
    }

    public void setPromotion_id(int promotion_id) {
        this.promotion_id = promotion_id;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public PromotionDetailRequest(int promotion_id, int limit, int offset) {
        this.promotion_id = promotion_id;
        this.limit = limit;
        this.offset = offset;
    }
}
