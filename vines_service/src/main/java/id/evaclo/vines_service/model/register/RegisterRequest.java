package id.evaclo.vines_service.model.register;

public class RegisterRequest {
    String fullname;    //"fullname": "jhonDoe",
    String email;       //"email":"24.00.pm@gmail.com",
    String password;    //"password":"rama100%",
    String phone;       //"phone":"081225192753",
    String token_id;    //"token_id":"12345jjkkllasdffnnnnnnnnnnnnnnn",
    String langitude;   //"longitude":"-6.2732980",
    String latitude;    //"latitude":"106.8694690"

    public RegisterRequest(String fullname, String email, String password, String phone, String token_id, String langitude, String latitude) {
        this.fullname = fullname;
        this.email = email;
        this.password = password;
        this.phone = phone;
        this.token_id = token_id;
        this.langitude = langitude;
        this.latitude = latitude;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getToken_id() {
        return token_id;
    }

    public void setToken_id(String token_id) {
        this.token_id = token_id;
    }

    public String getLangitude() {
        return langitude;
    }

    public void setLangitude(String langitude) {
        this.langitude = langitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }
}
