package id.evaclo.vines_service.model.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import id.evaclo.vines_service.core.BaseModel;

public class LoginResponse extends BaseModel {

    @Expose
    @SerializedName("time")
    private String time;
    @Expose
    @SerializedName("data")
    private Data data;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
        @Expose
        @SerializedName("user_data")
        private List<User_data> user_data;
        @Expose
        @SerializedName("token")
        private String token;

        public List<User_data> getUser_data() {
            return user_data;
        }

        public void setUser_data(List<User_data> user_data) {
            this.user_data = user_data;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }
    }

    public static class User_data {
        @Expose
        @SerializedName("date_join")
        private String date_join;
        @Expose
        @SerializedName("password")
        private String password;
        @Expose
        @SerializedName("foto")
        private String foto;
        @Expose
        @SerializedName("latitude")
        private double latitude;
        @Expose
        @SerializedName("longitude")
        private double longitude;
        @Expose
        @SerializedName("point")
        private int point;
        @Expose
        @SerializedName("email")
        private String email;
        @Expose
        @SerializedName("id_profile")
        private String id_profile;
        @Expose
        @SerializedName("fullname")
        private String fullname;
        @Expose
        @SerializedName("user_id")
        private int user_id;
        @Expose
        @SerializedName("phone")
        private String phone;
        @Expose
        @SerializedName("dob")
        private String dob;

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getDate_join() {
            return date_join;
        }

        public void setDate_join(String date_join) {
            this.date_join = date_join;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getFoto() {
            return foto;
        }

        public void setFoto(String foto) {
            this.foto = foto;
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        public int getPoint() {
            return point;
        }

        public void setPoint(int point) {
            this.point = point;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getId_profile() {
            return id_profile;
        }

        public void setId_profile(String id_profile) {
            this.id_profile = id_profile;
        }

        public String getFullname() {
            return fullname;
        }

        public void setFullname(String fullname) {
            this.fullname = fullname;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }
    }
}
