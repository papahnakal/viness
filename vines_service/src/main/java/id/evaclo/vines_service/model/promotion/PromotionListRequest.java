package id.evaclo.vines_service.model.promotion;

public class PromotionListRequest {
    private int limit;

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public PromotionListRequest(int limit) {
        this.limit = limit;
    }
}
