package id.evaclo.vines_service.model.register;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import id.evaclo.vines_service.core.BaseModel;

public class RegisterResponse extends BaseModel {

    @Expose
    @SerializedName("time")
    private String time;
    @Expose
    @SerializedName("data")
    private String data;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

}
