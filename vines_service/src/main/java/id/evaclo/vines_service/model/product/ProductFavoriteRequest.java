package id.evaclo.vines_service.model.product;

public class ProductFavoriteRequest {
    int limit;

    public ProductFavoriteRequest(int limit) {
        this.limit = limit;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }
}
