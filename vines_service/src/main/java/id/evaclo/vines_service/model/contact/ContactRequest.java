package id.evaclo.vines_service.model.contact;

public class ContactRequest {
    String email;
    String phone;
    String comment;

    public ContactRequest(String email, String phone, String comment) {
        this.email = email;
        this.phone = phone;
        this.comment = comment;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
