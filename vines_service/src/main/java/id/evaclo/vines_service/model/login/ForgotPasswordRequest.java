package id.evaclo.vines_service.model.login;

public class ForgotPasswordRequest {
    String email;           //"email": "mijan@botol.id",
    String new_password;    //"new_password": "botolmijan"

    public ForgotPasswordRequest(String email, String new_password) {
        this.email = email;
        this.new_password = new_password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNew_password() {
        return new_password;
    }

    public void setNew_password(String new_password) {
        this.new_password = new_password;
    }
}
