package id.evaclo.vines_service.model.wishlist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import id.evaclo.vines_service.core.BaseModel;

public class AddWishListResponse extends BaseModel {


    @Expose
    @SerializedName("time")
    private String time;
    @Expose
    @SerializedName("data")
    private Data data;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
        @Expose
        @SerializedName("changedRows")
        private int changedRows;
        @Expose
        @SerializedName("protocol41")
        private boolean protocol41;
        @Expose
        @SerializedName("message")
        private String message;
        @Expose
        @SerializedName("warningCount")
        private int warningCount;
        @Expose
        @SerializedName("serverStatus")
        private int serverStatus;
        @Expose
        @SerializedName("insertId")
        private int insertId;
        @Expose
        @SerializedName("affectedRows")
        private int affectedRows;
        @Expose
        @SerializedName("fieldCount")
        private int fieldCount;

        public int getChangedRows() {
            return changedRows;
        }

        public void setChangedRows(int changedRows) {
            this.changedRows = changedRows;
        }

        public boolean getProtocol41() {
            return protocol41;
        }

        public void setProtocol41(boolean protocol41) {
            this.protocol41 = protocol41;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public int getWarningCount() {
            return warningCount;
        }

        public void setWarningCount(int warningCount) {
            this.warningCount = warningCount;
        }

        public int getServerStatus() {
            return serverStatus;
        }

        public void setServerStatus(int serverStatus) {
            this.serverStatus = serverStatus;
        }

        public int getInsertId() {
            return insertId;
        }

        public void setInsertId(int insertId) {
            this.insertId = insertId;
        }

        public int getAffectedRows() {
            return affectedRows;
        }

        public void setAffectedRows(int affectedRows) {
            this.affectedRows = affectedRows;
        }

        public int getFieldCount() {
            return fieldCount;
        }

        public void setFieldCount(int fieldCount) {
            this.fieldCount = fieldCount;
        }
    }
}
