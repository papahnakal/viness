package id.evaclo.vines_service.model.news;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import id.evaclo.vines_service.core.BaseModel;

public class NewsDetailResponse extends BaseModel {

    @Expose
    @SerializedName("time")
    private String time;
    @Expose
    @SerializedName("data")
    private List<Data> data;


    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public static class Data {
        @Expose
        @SerializedName("datecreate")
        private String datecreate;
        @Expose
        @SerializedName("image")
        private String image;
        @Expose
        @SerializedName("description")
        private String description;
        @Expose
        @SerializedName("summary")
        private String summary;
        @Expose
        @SerializedName("title")
        private String title;
        @Expose
        @SerializedName("news_id")
        private int news_id;

        public String getDatecreate() {
            return datecreate;
        }

        public void setDatecreate(String datecreate) {
            this.datecreate = datecreate;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getSummary() {
            return summary;
        }

        public void setSummary(String summary) {
            this.summary = summary;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getNews_id() {
            return news_id;
        }

        public void setNews_id(int news_id) {
            this.news_id = news_id;
        }
    }
}
