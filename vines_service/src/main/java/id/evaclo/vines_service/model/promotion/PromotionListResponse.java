package id.evaclo.vines_service.model.promotion;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import id.evaclo.vines_service.core.BaseModel;

public class PromotionListResponse extends BaseModel {

    @Expose
    @SerializedName("time")
    private String time;
    @Expose
    @SerializedName("data")
    private List<Data> data;


    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public static class Data {
        @Expose
        @SerializedName("datecreate")
        private String datecreate;
        @Expose
        @SerializedName("status")
        private int status;
        @Expose
        @SerializedName("log")
        private int log;
        @Expose
        @SerializedName("summary")
        private String summary;
        @Expose
        @SerializedName("image")
        private String image;
        @Expose
        @SerializedName("title")
        private String title;
        @Expose
        @SerializedName("promotion_id")
        private int promotion_id;

        public String getDatecreate() {
            return datecreate;
        }

        public void setDatecreate(String datecreate) {
            this.datecreate = datecreate;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public int getLog() {
            return log;
        }

        public void setLog(int log) {
            this.log = log;
        }

        public String getSummary() {
            return summary;
        }

        public void setSummary(String summary) {
            this.summary = summary;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getPromotion_id() {
            return promotion_id;
        }

        public void setPromotion_id(int promotion_id) {
            this.promotion_id = promotion_id;
        }
    }
}
