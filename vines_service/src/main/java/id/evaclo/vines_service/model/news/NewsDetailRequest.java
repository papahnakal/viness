package id.evaclo.vines_service.model.news;

public class NewsDetailRequest {
    private int news_id;

    public NewsDetailRequest(int news_id) {
        this.news_id = news_id;
    }

    public int getNews_id() {
        return news_id;
    }

    public void setNews_id(int news_id) {
        this.news_id = news_id;
    }
}
