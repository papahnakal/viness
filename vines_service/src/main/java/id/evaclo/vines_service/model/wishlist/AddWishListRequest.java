package id.evaclo.vines_service.model.wishlist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddWishListRequest {

    @Expose
    @SerializedName("token")
    private String token;
    @Expose
    @SerializedName("product_id")
    private int product_id;
    @Expose
    @SerializedName("user_id")
    private int user_id;
    @Expose
    @SerializedName("store_id")
    private int store_id;

    public AddWishListRequest(String token, int product_id, int user_id,int store_id) {
        this.token = token;
        this.product_id = product_id;
        this.user_id = user_id;
        this.store_id = store_id;
    }

    public int getStore_id() {
        return store_id;
    }

    public void setStore_id(int store_id) {
        this.store_id = store_id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
}
