package id.evaclo.vines_service.model.product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductDetailRequest {


    @Expose
    @SerializedName("StoreId")
    private String StoreId;
    @Expose
    @SerializedName("product_id")
    private int product_id;

    public ProductDetailRequest(String storeId, int product_id) {
        StoreId = storeId;
        this.product_id = product_id;
    }

    public String getStoreId() {
        return StoreId;
    }

    public void setStoreId(String StoreId) {
        this.StoreId = StoreId;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }
}
