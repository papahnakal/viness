package id.evaclo.vines_service.model.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdatePasswordRequest {

    @Expose
    @SerializedName("token")
    private String token;
    @Expose
    @SerializedName("new_password")
    private String new_password;
    @Expose
    @SerializedName("email")
    private String email;

    public UpdatePasswordRequest(String token, String new_password, String email) {
        this.token = token;
        this.new_password = new_password;
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getNew_password() {
        return new_password;
    }

    public void setNew_password(String new_password) {
        this.new_password = new_password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
