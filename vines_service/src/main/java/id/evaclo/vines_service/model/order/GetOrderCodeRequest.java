package id.evaclo.vines_service.model.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetOrderCodeRequest {

    @Expose
    @SerializedName("token")
    private String token;
    @Expose
    @SerializedName("user_id")
    private String user_id;

    public GetOrderCodeRequest(String token, String user_id) {
        this.token = token;
        this.user_id = user_id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
