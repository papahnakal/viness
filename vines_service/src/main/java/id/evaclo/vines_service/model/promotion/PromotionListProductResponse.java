package id.evaclo.vines_service.model.promotion;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import id.evaclo.vines_service.core.BaseModel;

public class PromotionListProductResponse extends BaseModel {

    @Expose
    @SerializedName("time")
    private String time;
    @Expose
    @SerializedName("data")
    private List<Data> data;


    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public static class Data {
        @Expose
        @SerializedName("datecreate")
        private String datecreate;
        @Expose
        @SerializedName("category_name")
        private String category_name;
        @Expose
        @SerializedName("category_id")
        private int category_id;
        @Expose
        @SerializedName("promotion_discount")
        private int promotion_discount;
        @Expose
        @SerializedName("promotion_title")
        private String promotion_title;
        @Expose
        @SerializedName("promotion_price")
        private int promotion_price;
        @Expose
        @SerializedName("summary")
        private String summary;
        @Expose
        @SerializedName("image")
        private String image;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("product_id")
        private int product_id;

        public String getDatecreate() {
            return datecreate;
        }

        public void setDatecreate(String datecreate) {
            this.datecreate = datecreate;
        }

        public String getCategory_name() {
            return category_name;
        }

        public void setCategory_name(String category_name) {
            this.category_name = category_name;
        }

        public int getCategory_id() {
            return category_id;
        }

        public void setCategory_id(int category_id) {
            this.category_id = category_id;
        }

        public int getPromotion_discount() {
            return promotion_discount;
        }

        public void setPromotion_discount(int promotion_discount) {
            this.promotion_discount = promotion_discount;
        }

        public String getPromotion_title() {
            return promotion_title;
        }

        public void setPromotion_title(String promotion_title) {
            this.promotion_title = promotion_title;
        }

        public int getPromotion_price() {
            return promotion_price;
        }

        public void setPromotion_price(int promotion_price) {
            this.promotion_price = promotion_price;
        }

        public String getSummary() {
            return summary;
        }

        public void setSummary(String summary) {
            this.summary = summary;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getProduct_id() {
            return product_id;
        }

        public void setProduct_id(int product_id) {
            this.product_id = product_id;
        }
    }
}
