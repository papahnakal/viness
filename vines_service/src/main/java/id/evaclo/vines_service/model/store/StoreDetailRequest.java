package id.evaclo.vines_service.model.store;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StoreDetailRequest {

    @Expose
    @SerializedName("store_id")
    private int store_id;

    public StoreDetailRequest(int store_id) {
        this.store_id = store_id;
    }

    public int getStore_id() {
        return store_id;
    }

    public void setStore_id(int store_id) {
        this.store_id = store_id;
    }
}
