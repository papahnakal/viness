package id.evaclo.vines_service.model.product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductListRequest {

    @Expose
    @SerializedName("price")
    private String price;
    @Expose
    @SerializedName("country")
    private String country;
    @Expose
    @SerializedName("sort")
    private String sort;
    @Expose
    @SerializedName("user_id")
    private int user_id;
    @Expose
    @SerializedName("offset")
    private int offset;
    @Expose
    @SerializedName("category_id")
    private String category_id;
    @Expose
    @SerializedName("limit")
    private int limit;
    @Expose
    @SerializedName("store_id")
    private int store_id;
    @Expose
    @SerializedName("keyword")
    private String keyword;

    public ProductListRequest(String keyword,String price, String country, String sort, int user_id, int offset, String category_id, int limit, int store_id) {
        this.price = price;
        this.country = country;
        this.sort = sort;
        this.user_id = user_id;
        this.offset = offset;
        this.category_id = category_id;
        this.limit = limit;
        this.store_id = store_id;
        this.keyword = keyword;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getStore_id() {
        return store_id;
    }

    public void setStore_id(int store_id) {
        this.store_id = store_id;
    }
}
