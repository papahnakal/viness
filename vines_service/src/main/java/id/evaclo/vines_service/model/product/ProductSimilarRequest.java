package id.evaclo.vines_service.model.product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductSimilarRequest {

    @Expose
    @SerializedName("user_id")
    private int user_id;
    @Expose
    @SerializedName("offset")
    private int offset;
    @Expose
    @SerializedName("limit")
    private int limit;
    @Expose
    @SerializedName("by_title")
    private String by_title;
    @Expose
    @SerializedName("category_id")
    private int category_id;
    @Expose
    @SerializedName("store_id")
    private int store_id;

    public ProductSimilarRequest(int user_id, int offset, int limit, String by_title, int category_id, int store_id) {
        this.offset = offset;
        this.limit = limit;
        this.by_title = by_title;
        this.category_id = category_id;
        this.store_id = store_id;
        this.user_id = user_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getStore_id() {
        return store_id;
    }

    public void setStore_id(int store_id) {
        this.store_id = store_id;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String getBy_title() {
        return by_title;
    }

    public void setBy_title(String by_title) {
        this.by_title = by_title;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }
}
