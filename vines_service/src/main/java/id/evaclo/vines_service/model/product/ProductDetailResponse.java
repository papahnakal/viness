package id.evaclo.vines_service.model.product;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import id.evaclo.vines_service.core.BaseModel;

public class ProductDetailResponse extends BaseModel implements Parcelable {


    @Expose
    @SerializedName("time")
    private String time;
    @Expose
    @SerializedName("data")
    private List<Data> data;

    protected ProductDetailResponse(Parcel in) {
        time = in.readString();
    }

    public static final Creator<ProductDetailResponse> CREATOR = new Creator<ProductDetailResponse>() {
        @Override
        public ProductDetailResponse createFromParcel(Parcel in) {
            return new ProductDetailResponse(in);
        }

        @Override
        public ProductDetailResponse[] newArray(int size) {
            return new ProductDetailResponse[size];
        }
    };

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(time);
    }

    public static class Data implements Parcelable{
        @Expose
        @SerializedName("datecreate")
        private String datecreate;
        @Expose
        @SerializedName("category_name")
        private String category_name;
        @Expose
        @SerializedName("image")
        private String image;
        @Expose
        @SerializedName("sub_region")
        private String sub_region;
        @Expose
        @SerializedName("abv")
        private double abv;
        @Expose
        @SerializedName("size")
        private List<Size> size;
        @Expose
        @SerializedName("discount")
        private int discount;
        @Expose
        @SerializedName("stock")
        private int stock;
        @Expose
        @SerializedName("price")
        private int price;
        @Expose
        @SerializedName("summary")
        private String summary;
        @Expose
        @SerializedName("code")
        private String code;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("category_id")
        private int category_id;
        @Expose
        @SerializedName("product_id")
        private int product_id;

        public Data(String datecreate, String category_name, String image, String sub_region, int abv, List<Size> size, int discount, int stock, int price, String summary, String code, String name, int category_id, int product_id) {
            this.datecreate = datecreate;
            this.category_name = category_name;
            this.image = image;
            this.sub_region = sub_region;
            this.abv = abv;
            this.size = size;
            this.discount = discount;
            this.stock = stock;
            this.price = price;
            this.summary = summary;
            this.code = code;
            this.name = name;
            this.category_id = category_id;
            this.product_id = product_id;
        }


        protected Data(Parcel in) {
            datecreate = in.readString();
            category_name = in.readString();
            image = in.readString();
            sub_region = in.readString();
            abv = in.readDouble();
            size = in.createTypedArrayList(Size.CREATOR);
            discount = in.readInt();
            stock = in.readInt();
            price = in.readInt();
            summary = in.readString();
            code = in.readString();
            name = in.readString();
            category_id = in.readInt();
            product_id = in.readInt();
        }

        public static final Creator<Data> CREATOR = new Creator<Data>() {
            @Override
            public Data createFromParcel(Parcel in) {
                return new Data(in);
            }

            @Override
            public Data[] newArray(int size) {
                return new Data[size];
            }
        };

        public String getDatecreate() {
            return datecreate;
        }

        public void setDatecreate(String datecreate) {
            this.datecreate = datecreate;
        }

        public String getCategory_name() {
            return category_name;
        }

        public void setCategory_name(String category_name) {
            this.category_name = category_name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getSub_region() {
            return sub_region;
        }

        public void setSub_region(String sub_region) {
            this.sub_region = sub_region;
        }

        public double getAbv() {
            return abv;
        }

        public void setAbv(int abv) {
            this.abv = abv;
        }

        public List<Size> getSize() {
            return size;
        }

        public void setSize(List<Size> size) {
            this.size = size;
        }

        public int getDiscount() {
            return discount;
        }

        public void setDiscount(int discount) {
            this.discount = discount;
        }

        public int getStock() {
            return stock;
        }

        public void setStock(int stock) {
            this.stock = stock;
        }

        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }

        public String getSummary() {
            return summary;
        }

        public void setSummary(String summary) {
            this.summary = summary;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getCategory_id() {
            return category_id;
        }

        public void setCategory_id(int category_id) {
            this.category_id = category_id;
        }

        public int getProduct_id() {
            return product_id;
        }

        public void setProduct_id(int product_id) {
            this.product_id = product_id;
        }


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(datecreate);
            dest.writeString(category_name);
            dest.writeString(image);
            dest.writeString(sub_region);
            dest.writeDouble(abv);
            dest.writeTypedList(size);
            dest.writeInt(discount);
            dest.writeInt(stock);
            dest.writeInt(price);
            dest.writeString(summary);
            dest.writeString(code);
            dest.writeString(name);
            dest.writeInt(category_id);
            dest.writeInt(product_id);
        }
    }

    public static class Size implements Parcelable{
        @Expose
        @SerializedName("stock")
        private int stock;
        @Expose
        @SerializedName("price")
        private int price;
        @Expose
        @SerializedName("code")
        private String code;
        @Expose
        @SerializedName("name")
        private int name;

        protected Size(Parcel in) {
            stock = in.readInt();
            price = in.readInt();
            code = in.readString();
            name = in.readInt();
        }

        public static final Creator<Size> CREATOR = new Creator<Size>() {
            @Override
            public Size createFromParcel(Parcel in) {
                return new Size(in);
            }

            @Override
            public Size[] newArray(int size) {
                return new Size[size];
            }
        };

        public int getStock() {
            return stock;
        }

        public void setStock(int stock) {
            this.stock = stock;
        }

        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public int getName() {
            return name;
        }

        public void setName(int name) {
            this.name = name;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(stock);
            dest.writeInt(price);
            dest.writeString(code);
            dest.writeInt(name);
        }
    }
}
