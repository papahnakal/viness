package id.evaclo.vines_service.view;

import id.evaclo.vines_service.core.BaseView;
import id.evaclo.vines_service.model.TermResponse;
import id.evaclo.vines_service.model.about.AboutResponse;
import id.evaclo.vines_service.model.contact.ContactResponse;

public interface AboutView extends BaseView {
    void failure(String message);
    void showMessage(String message);
    void onSuccessGetAbout(AboutResponse response);
    void onSuccessGetTerm(TermResponse response);
    void onSuccessGetPrivateInfo(TermResponse response);
    void onSuccessPostCoomment(ContactResponse response);
}
