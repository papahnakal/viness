package id.evaclo.vines_service.view;

import id.evaclo.vines_service.core.BaseView;
import id.evaclo.vines_service.model.promotion.PromotionListResponse;
import id.evaclo.vines_service.model.store.StoreListResponse;

public interface HomepageView extends BaseView {
    void failure(String message);
    void showMessage(String message);
    void onSuccessGetListPromotion(PromotionListResponse response);
    void onSuccessGetListStore(StoreListResponse response);
}
