package id.evaclo.vines_service.view;

import id.evaclo.vines_service.core.BaseView;
import id.evaclo.vines_service.model.news.NewsDetailResponse;
import id.evaclo.vines_service.model.news.NewsListResponse;

public interface NewsView extends BaseView {
    void failure(String message);
    void showMessage(String message);
    void onSuccessGetListNews(NewsListResponse response);
    void onSuccessGetDetailtNews(NewsDetailResponse response);
}
