package id.evaclo.vines_service.view;

import id.evaclo.vines_service.core.BaseView;
import id.evaclo.vines_service.model.product.ProductDetailResponse;
import id.evaclo.vines_service.model.product.ProductFavoriteListResponse;
import id.evaclo.vines_service.model.product.ProductListResponse;
import id.evaclo.vines_service.model.product.ProductSimilarListResponse;

public interface ProductView extends BaseView {
    void failure(String message);
    void showMessage(String message);
    void onSuccessGetListProduct(ProductListResponse response);
    void onSuccessGetSimilarProduct(ProductSimilarListResponse response);
    void onSuccessGetFavoriteProduct(ProductFavoriteListResponse response);
    void onSuccessGetDetailProduct(ProductDetailResponse response);
    void onSuccessAddWishlist(String message);
}
