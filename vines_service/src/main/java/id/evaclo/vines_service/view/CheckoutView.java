package id.evaclo.vines_service.view;

import id.evaclo.vines_service.core.BaseView;
import id.evaclo.vines_service.model.order.GetOrderCodeResponse;

public interface CheckoutView extends BaseView {
    void onFailure(String message);
    void onSuccessGetOrderId(GetOrderCodeResponse response);
}
