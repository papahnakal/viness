package id.evaclo.vines_service.view;

import id.evaclo.vines_service.core.BaseView;
import id.evaclo.vines_service.model.order.RecentOrderResponse;

public interface ProfileView extends BaseView {
    void onSuccessGetRecentOrder(RecentOrderResponse response);
    void failure(String message);
}
