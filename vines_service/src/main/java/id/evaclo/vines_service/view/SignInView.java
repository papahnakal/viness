package id.evaclo.vines_service.view;

import id.evaclo.vines_service.core.BaseView;
import id.evaclo.vines_service.model.login.LoginResponse;

public interface SignInView extends BaseView {
    void onSuccess(LoginResponse response);
    void onFailed(String message);
    void showMessage(String message);
    void showProgress();
    void hideProgress();
}
