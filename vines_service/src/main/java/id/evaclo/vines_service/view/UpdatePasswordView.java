package id.evaclo.vines_service.view;

import id.evaclo.vines_service.core.BaseView;
import id.evaclo.vines_service.model.login.UpdatePasswordResponse;

public interface UpdatePasswordView extends BaseView {
    void onSuccessUpdatePassword(UpdatePasswordResponse response);
    void onFailure(String message);
}
