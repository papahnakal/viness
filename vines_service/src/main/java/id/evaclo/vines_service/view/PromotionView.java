package id.evaclo.vines_service.view;

import id.evaclo.vines_service.core.BaseView;
import id.evaclo.vines_service.model.promotion.PromotionListProductResponse;
import id.evaclo.vines_service.model.promotion.PromotionListResponse;

public interface PromotionView extends BaseView {
    void failure(String message);
    void showMessage(String message);
    void onSuccessGetListPromotion(PromotionListResponse response);
    void onSuccessGetDetailPromotion(PromotionListProductResponse response);
}
