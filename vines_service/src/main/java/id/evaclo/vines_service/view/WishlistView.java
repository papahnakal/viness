package id.evaclo.vines_service.view;

import id.evaclo.vines_service.core.BaseView;
import id.evaclo.vines_service.model.wishlist.WishListResponse;

public interface WishlistView extends BaseView {
    void onSuccessGetWishList(WishListResponse response);
    void failure(String message);
    void showMessage(String message);
}
