package id.evaclo.vines_service.view;


import id.evaclo.vines_service.core.BaseView;
import id.evaclo.vines_service.model.register.RegisterResponse;

public interface SignUpView extends BaseView {
    void onSuccess(RegisterResponse response);
    void onFailed(String message);
    void showMessage(String message);
    void showProgress();
    void hideProgress();
}
