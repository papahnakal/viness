package id.evaclo.vines_service.view;

import id.evaclo.vines_service.core.BaseView;
import id.evaclo.vines_service.model.PartyResponse;

public interface PartyView extends BaseView {
    void failure(String message);
    void showMessage(String message);
    void onSuccessPostParty(PartyResponse response);
}
