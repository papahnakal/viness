package id.evaclo.vines_service.view;

import id.evaclo.vines_service.core.BaseView;
import id.evaclo.vines_service.model.login.EditProfileResponse;

public interface EditProfileView extends BaseView {
    void onSuccessEditProfile(EditProfileResponse response);
    void onFailure(String message);
}
