package id.evaclo.vines_service.view;

import id.evaclo.vines_service.core.BaseView;
import id.evaclo.vines_service.model.order.HistoryOrderResponse;
import id.evaclo.vines_service.model.order.RecentOrderResponse;

public interface OrderView extends BaseView {
    void failure(String message);
    void showMessage(String message);
    void onSuccessGetHistoryOrder(HistoryOrderResponse response);
    void onSuccessGetRecentOrder(RecentOrderResponse response);
}
