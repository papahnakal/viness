package id.evaclo.vines_service.view;

import id.evaclo.vines_service.core.BaseView;
import id.evaclo.vines_service.model.store.StoreDetailResponse;
import id.evaclo.vines_service.model.store.StoreListResponse;

public interface StoreView extends BaseView {
    void failure(String message);
    void showMessage(String message);
    void onSuccessGetListStore(StoreListResponse response);
    void onSuccessGetDetailStore(StoreDetailResponse response);
}
