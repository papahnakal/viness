package id.evaclo.vines_service.presenter;

import android.content.Context;

import id.evaclo.vines_service.BuildConfig;
import id.evaclo.vines_service.core.BasePresenter;
import id.evaclo.vines_service.model.order.RecentOrderRequest;
import id.evaclo.vines_service.model.order.RecentOrderResponse;
import id.evaclo.vines_service.service.VinesServices;
import id.evaclo.vines_service.service.VinesSession;
import id.evaclo.vines_service.view.ProfileView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfilePresenter extends BasePresenter<ProfileView> {
    Context context;
    VinesSession vinesSession;

    public ProfilePresenter(Context context, VinesSession vinesSession) {
        this.context = context;
        this.vinesSession = vinesSession;
    }
    public void getRecentOrder(String token, int offset, int limit, int user_id){
        RecentOrderRequest request = new RecentOrderRequest(token,offset,limit,user_id);
        Call<RecentOrderResponse> call = VinesServices.createWithAuth(BuildConfig.BASE_URL,vinesSession).getRecentOrder(request);
        call.enqueue(new Callback<RecentOrderResponse>() {
            @Override
            public void onResponse(Call<RecentOrderResponse> call, Response<RecentOrderResponse> response) {
                if(response.isSuccessful()){
                    if(response.body().getStatus() == 200){
                        getMvpView().onSuccessGetRecentOrder(response.body());
                    }else {
                        getMvpView().failure(response.message());
                    }
                }else{
                    getMvpView().failure(response.message());
                }
            }

            @Override
            public void onFailure(Call<RecentOrderResponse> call, Throwable t) {
                getMvpView().failure(t.getLocalizedMessage());
            }
        });
    }
}
