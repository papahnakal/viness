package id.evaclo.vines_service.presenter;

import android.content.Context;

import id.evaclo.vines_service.BuildConfig;
import id.evaclo.vines_service.core.BasePresenter;
import id.evaclo.vines_service.core.BaseView;
import id.evaclo.vines_service.model.wishlist.AddWishListRequest;
import id.evaclo.vines_service.model.wishlist.AddWishListResponse;
import id.evaclo.vines_service.model.wishlist.WishListRequest;
import id.evaclo.vines_service.model.wishlist.WishListResponse;
import id.evaclo.vines_service.service.VinesServices;
import id.evaclo.vines_service.service.VinesSession;
import id.evaclo.vines_service.view.WishlistView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WishListPresenter extends BasePresenter<WishlistView> {
    Context context;
    VinesSession vinesSession;

    public WishListPresenter(Context context, VinesSession vinesSession) {
        this.context = context;
        this.vinesSession = vinesSession;
    }

    public void getListWishlist(String token, int offset, int limit, int user_id){
        WishListRequest request = new WishListRequest(token,offset,limit,user_id);
        Call<WishListResponse>call = VinesServices.createWithAuth(BuildConfig.BASE_URL,vinesSession).getListWishList(request);
        call.enqueue(new Callback<WishListResponse>() {
            @Override
            public void onResponse(Call<WishListResponse> call, Response<WishListResponse> response) {
                if(response.isSuccessful()){
                    if(response.body().getStatus() == 200){
                        getMvpView().onSuccessGetWishList(response.body());
                    }else {
                        getMvpView().failure(response.message());
                    }
                }else{
                    getMvpView().failure(response.message());
                }
            }

            @Override
            public void onFailure(Call<WishListResponse> call, Throwable t) {
                getMvpView().failure(t.getLocalizedMessage());
            }
        });
    }
    public void addWishList(String token, int product_id, int user_id,int store_id){
        AddWishListRequest request = new AddWishListRequest(token,product_id,user_id,store_id);
        Call<AddWishListResponse> call = VinesServices.createWithAuth(BuildConfig.BASE_URL,vinesSession).deleteWishList(request);
        call.enqueue(new Callback<AddWishListResponse>() {
            @Override
            public void onResponse(Call<AddWishListResponse> call, Response<AddWishListResponse> response) {
                if(response.isSuccessful()){
                    if(response.body().getStatus() == 200){
                        getMvpView().showMessage(response.body().getDisplay_message());
                    }else {
                        getMvpView().failure(response.message());
                    }
                }else{
                    getMvpView().failure(response.message());
                }
            }

            @Override
            public void onFailure(Call<AddWishListResponse> call, Throwable t) {
                getMvpView().failure(t.getLocalizedMessage());
            }
        });
    }
}
