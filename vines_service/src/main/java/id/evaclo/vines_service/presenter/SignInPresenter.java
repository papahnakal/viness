package id.evaclo.vines_service.presenter;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import id.evaclo.vines_service.BuildConfig;
import id.evaclo.vines_service.core.BasePresenter;
import id.evaclo.vines_service.model.login.AuthenticateRequest;
import id.evaclo.vines_service.model.login.LoginResponse;
import id.evaclo.vines_service.service.Header;
import id.evaclo.vines_service.service.VinesServices;
import id.evaclo.vines_service.service.VinesSession;
import id.evaclo.vines_service.view.SignInView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignInPresenter extends BasePresenter<SignInView> {
    Context context;
    VinesSession vinesSession;

    public SignInPresenter(Context context, VinesSession vinesSession) {
        this.context = context;
        this.vinesSession = vinesSession;
    }

    public List<Header> setHeaders(){
        Header header1 = new Header("Content-Type", "application/json");
        Header header2 = new Header("apikey", BuildConfig.APIKEY);
        List<Header> headers = new ArrayList();
        headers.add(header1);
        headers.add(header2);
        return headers;
    }
    public void getAuthentication(String email,String password){
        //AuthenticateRequest request = new AuthenticateRequest("24.00.pm@gmail.id","rama100%");
        AuthenticateRequest request = new AuthenticateRequest(email,password);
        Call<LoginResponse> call = VinesServices.createWithAuth(BuildConfig.BASE_URL,vinesSession).getAuthentication(request);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    if(response.isSuccessful()){
                        LoginResponse loginResponse = response.body();
                        if(loginResponse.getStatus()==200) {
                            getMvpView().onSuccess(response.body());
                            getMvpView().showMessage(response.message());
                            vinesSession.storeResponse(response.body());
                        }else{
                            getMvpView().onFailed(response.body().getDisplay_message());
                            getMvpView().showMessage(response.body().getDisplay_message());
                        }
                    }else{
                        getMvpView().onFailed(response.body().getDisplay_message());
                        getMvpView().showMessage(response.body().getDisplay_message());
                    }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                    getMvpView().onFailed(t.getLocalizedMessage());
            }
        });

    }

}
