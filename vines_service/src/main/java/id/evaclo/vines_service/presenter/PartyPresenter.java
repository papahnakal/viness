package id.evaclo.vines_service.presenter;

import android.content.Context;

import id.evaclo.vines_service.BuildConfig;
import id.evaclo.vines_service.core.BasePresenter;
import id.evaclo.vines_service.model.PartyRequest;
import id.evaclo.vines_service.model.PartyResponse;
import id.evaclo.vines_service.model.contact.ContactResponse;
import id.evaclo.vines_service.service.VinesServices;
import id.evaclo.vines_service.service.VinesSession;
import id.evaclo.vines_service.view.PartyView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PartyPresenter extends BasePresenter<PartyView> {
    Context context;
    VinesSession vinesSession;

    public PartyPresenter(Context context, VinesSession vinesSession) {
        this.context = context;
        this.vinesSession = vinesSession;
    }
    public void postParty(String first_name, String last_name, String email, String phone, String location, String budget, String person, String date_party, String category){
        PartyRequest request = new PartyRequest(first_name,last_name,email,phone,location,budget,person,date_party,category);
        Call<PartyResponse> call = VinesServices.createWithAuth(BuildConfig.BASE_URL,vinesSession).postParty(request);
        call.enqueue(new Callback<PartyResponse>() {
            @Override
            public void onResponse(Call<PartyResponse> call, Response<PartyResponse> response) {
                if(response.isSuccessful()){
                    if(response.body().getStatus() == 200){
                        getMvpView().onSuccessPostParty(response.body());
                    }else {
                        getMvpView().failure(response.message());
                    }
                }else{
                    getMvpView().failure(response.message());
                }
            }

            @Override
            public void onFailure(Call<PartyResponse> call, Throwable t) {
                getMvpView().failure(t.getLocalizedMessage());
            }
        });
    }
}
