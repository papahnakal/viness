package id.evaclo.vines_service.presenter;

import android.content.Context;
import android.os.Build;

import id.evaclo.vines_service.BuildConfig;
import id.evaclo.vines_service.core.BasePresenter;
import id.evaclo.vines_service.model.product.ProductDetailRequest;
import id.evaclo.vines_service.model.product.ProductDetailResponse;
import id.evaclo.vines_service.model.product.ProductFavoriteListResponse;
import id.evaclo.vines_service.model.product.ProductFavoriteRequest;
import id.evaclo.vines_service.model.product.ProductListRequest;
import id.evaclo.vines_service.model.product.ProductListResponse;
import id.evaclo.vines_service.model.product.ProductSimilarListResponse;
import id.evaclo.vines_service.model.product.ProductSimilarRequest;
import id.evaclo.vines_service.model.wishlist.AddWishListRequest;
import id.evaclo.vines_service.model.wishlist.AddWishListResponse;
import id.evaclo.vines_service.model.wishlist.WishListResponse;
import id.evaclo.vines_service.service.VinesServices;
import id.evaclo.vines_service.service.VinesSession;
import id.evaclo.vines_service.view.ProductView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductPresenter extends BasePresenter<ProductView> {
    Context context;
    VinesSession vinesSession;

    public ProductPresenter(Context context, VinesSession vinesSession) {
        this.context = context;
        this.vinesSession = vinesSession;
    }
    public void getListProduct( String keyword, String price, String country,int storeId,int offset,int limit, String category_id){
        ProductListRequest request = new ProductListRequest(
                keyword,
                price, country, "Az",
                vinesSession.getLoginResponse().getData().getUser_data().get(0).getUser_id(),
                offset, category_id, limit, storeId
                );
        Call<ProductListResponse> call = VinesServices.createWithAuth(BuildConfig.BASE_URL,vinesSession).getProductList(request);
        call.enqueue(new Callback<ProductListResponse>() {
            @Override
            public void onResponse(Call<ProductListResponse> call, Response<ProductListResponse> response) {
                if(response.isSuccessful()){
                    if(response.body().getStatus() == 200){
                        getMvpView().onSuccessGetListProduct(response.body());
                    }else {
                        getMvpView().failure(response.message());
                    }
                }else{
                    getMvpView().failure(response.message());
                }
            }

            @Override
            public void onFailure(Call<ProductListResponse> call, Throwable t) {
                getMvpView().failure(t.getLocalizedMessage());
            }
        });
    }
    public void getSimilarProduct(int user_id,int offset, int limit, String by_title, int category_id,int storeId){
        ProductSimilarRequest request = new ProductSimilarRequest(user_id,offset,limit,by_title,category_id,storeId);
        Call<ProductSimilarListResponse> call = VinesServices.createWithAuth(BuildConfig.BASE_URL,vinesSession).getProductSimilar(request);
        call.enqueue(new Callback<ProductSimilarListResponse>() {
            @Override
            public void onResponse(Call<ProductSimilarListResponse> call, Response<ProductSimilarListResponse> response) {
                if(response.isSuccessful()){
                    if(response.body().getStatus() == 200){
                        getMvpView().onSuccessGetSimilarProduct(response.body());
                    }else {
                        getMvpView().failure(response.message());
                    }
                }else{
                    getMvpView().failure(response.message());
                }
            }

            @Override
            public void onFailure(Call<ProductSimilarListResponse> call, Throwable t) {
                getMvpView().failure(t.getLocalizedMessage());
            }
        });
    }
    public void getFavoriteProduct(int limit){
        ProductFavoriteRequest request = new ProductFavoriteRequest(limit);
        Call<ProductFavoriteListResponse> call = VinesServices.createWithAuth(BuildConfig.BASE_URL,vinesSession).getProductFavorite(request);
        call.enqueue(new Callback<ProductFavoriteListResponse>() {
            @Override
            public void onResponse(Call<ProductFavoriteListResponse> call, Response<ProductFavoriteListResponse> response) {
                if(response.isSuccessful()){
                    if(response.body().getStatus() == 200){
                        getMvpView().onSuccessGetFavoriteProduct(response.body());
                    }else {
                        getMvpView().failure(response.message());
                    }
                }else{
                    getMvpView().failure(response.message());
                }
            }

            @Override
            public void onFailure(Call<ProductFavoriteListResponse> call, Throwable t) {
                getMvpView().failure(t.getLocalizedMessage());
            }
        });
    }
    public void getDetailProduct(String storeId,int product_id){
        ProductDetailRequest request = new ProductDetailRequest(storeId,product_id);
        Call<ProductDetailResponse> call = VinesServices.createWithAuth(BuildConfig.BASE_URL,vinesSession).getProductDetail(request);
        call.enqueue(new Callback<ProductDetailResponse>() {
            @Override
            public void onResponse(Call<ProductDetailResponse> call, Response<ProductDetailResponse> response) {
                if(response.isSuccessful()){
                    if(response.body().getStatus() == 200){
                        getMvpView().onSuccessGetDetailProduct(response.body());
                    }else {
                        getMvpView().failure(response.message());
                    }
                }else{
                    getMvpView().failure(response.message());
                }
            }

            @Override
            public void onFailure(Call<ProductDetailResponse> call, Throwable t) {
                getMvpView().failure(t.getLocalizedMessage());
            }
        });
    }
    public void addWishList(String token, int product_id, int user_id,int store_id){
        AddWishListRequest request = new AddWishListRequest(token,product_id,user_id,store_id);
        Call<AddWishListResponse> call = VinesServices.createWithAuth(BuildConfig.BASE_URL,vinesSession).addWishList(request);
        call.enqueue(new Callback<AddWishListResponse>() {
            @Override
            public void onResponse(Call<AddWishListResponse> call, Response<AddWishListResponse> response) {
                if(response.isSuccessful()){
                    if(response.body().getStatus() == 200){
                        getMvpView().showMessage(response.body().getDisplay_message());
                    }else {
                        getMvpView().failure(response.message());
                    }
                }else{
                    getMvpView().failure(response.message());
                }
            }

            @Override
            public void onFailure(Call<AddWishListResponse> call, Throwable t) {
                getMvpView().failure(t.getLocalizedMessage());
            }
        });
    }
}
