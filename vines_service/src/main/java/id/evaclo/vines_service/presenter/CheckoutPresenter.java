package id.evaclo.vines_service.presenter;

import android.content.Context;

import id.evaclo.vines_service.BuildConfig;
import id.evaclo.vines_service.core.BasePresenter;
import id.evaclo.vines_service.model.order.GetOrderCodeRequest;
import id.evaclo.vines_service.model.order.GetOrderCodeResponse;
import id.evaclo.vines_service.model.order.HistoryOrderResponse;
import id.evaclo.vines_service.service.VinesServices;
import id.evaclo.vines_service.service.VinesSession;
import id.evaclo.vines_service.view.CheckoutView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckoutPresenter extends BasePresenter<CheckoutView> {
    Context context;
    VinesSession vinesSession;

    public CheckoutPresenter(Context context, VinesSession vinesSession) {
        this.context = context;
        this.vinesSession = vinesSession;
    }

    public void getOrderId(int userId){
        GetOrderCodeRequest request = new GetOrderCodeRequest(vinesSession.getToken(),userId+"");
        Call<GetOrderCodeResponse> call = VinesServices.createWithAuth(BuildConfig.BASE_URL,vinesSession).getOrderCode(request);
        call.enqueue(new Callback<GetOrderCodeResponse>() {
            @Override
            public void onResponse(Call<GetOrderCodeResponse> call, Response<GetOrderCodeResponse> response) {
                if(response.isSuccessful()){
                    if(response.body().getStatus() == 200){
                        getMvpView().onSuccessGetOrderId(response.body());
                    }else {
                        getMvpView().onFailure(response.message());
                    }
                }else{
                    getMvpView().onFailure(response.message());
                }
            }

            @Override
            public void onFailure(Call<GetOrderCodeResponse> call, Throwable t) {
                getMvpView().onFailure(t.getLocalizedMessage());
            }
        });
    }
}
