package id.evaclo.vines_service.presenter;

import android.content.Context;

import id.evaclo.vines_service.BuildConfig;
import id.evaclo.vines_service.core.BasePresenter;
import id.evaclo.vines_service.model.TermResponse;
import id.evaclo.vines_service.model.about.AboutResponse;
import id.evaclo.vines_service.model.contact.ContactRequest;
import id.evaclo.vines_service.model.contact.ContactResponse;
import id.evaclo.vines_service.service.VinesServices;
import id.evaclo.vines_service.service.VinesSession;
import id.evaclo.vines_service.view.AboutView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AboutPresenter extends BasePresenter<AboutView> {
    Context context;
    VinesSession vinesSession;

    public AboutPresenter(Context context, VinesSession vinesSession) {
        this.context = context;
        this.vinesSession = vinesSession;
    }
    public void getAbout(){
        Call<AboutResponse> call = VinesServices.createWithAuth(BuildConfig.BASE_URL,vinesSession).getAbout();
        call.enqueue(new Callback<AboutResponse>() {
            @Override
            public void onResponse(Call<AboutResponse> call, Response<AboutResponse> response) {
                if(response.isSuccessful()){
                    if(response.body().getStatus() == 200){
                        getMvpView().onSuccessGetAbout(response.body());
                    }else {
                        getMvpView().failure(response.message());
                    }
                }else{
                    getMvpView().failure(response.message());
                }
            }

            @Override
            public void onFailure(Call<AboutResponse> call, Throwable t) {
                getMvpView().failure(t.getLocalizedMessage());
            }
        });
    }
    public void postContact(String email,String phone,String comment){
        ContactRequest request = new ContactRequest(email,phone,comment);
        Call<ContactResponse> call = VinesServices.createWithAuth(BuildConfig.BASE_URL,vinesSession).postContact(request);
        call.enqueue(new Callback<ContactResponse>() {
            @Override
            public void onResponse(Call<ContactResponse> call, Response<ContactResponse> response) {
                if(response.isSuccessful()){
                    if(response.body().getStatus() == 200){
                        getMvpView().onSuccessPostCoomment(response.body());
                    }else {
                        getMvpView().failure(response.message());
                    }
                }else{
                    getMvpView().failure(response.message());
                }
            }

            @Override
            public void onFailure(Call<ContactResponse> call, Throwable t) {
                getMvpView().failure(t.getLocalizedMessage());
            }
        });
    }
    public void postGetTerm(){
        Call<TermResponse> call = VinesServices.createWithAuth(BuildConfig.BASE_URL,vinesSession).postGetTermAndConfition();
        call.enqueue(new Callback<TermResponse>() {
            @Override
            public void onResponse(Call<TermResponse> call, Response<TermResponse> response) {
                if(response.isSuccessful()){
                    if(response.body().getStatus() == 200){
                        getMvpView().onSuccessGetTerm(response.body());
                    }else {
                        getMvpView().failure(response.message());
                    }
                }else{
                    getMvpView().failure(response.message());
                }
            }

            @Override
            public void onFailure(Call<TermResponse> call, Throwable t) {
                getMvpView().failure(t.getLocalizedMessage());
            }
        });
    }
    public void postGetPrivacy(){
        Call<TermResponse> call = VinesServices.createWithAuth(BuildConfig.BASE_URL,vinesSession).postGetPrivacy();
        call.enqueue(new Callback<TermResponse>() {
            @Override
            public void onResponse(Call<TermResponse> call, Response<TermResponse> response) {
                if(response.isSuccessful()){
                    if(response.body().getStatus() == 200){
                        getMvpView().onSuccessGetPrivateInfo(response.body());
                    }else {
                        getMvpView().failure(response.message());
                    }
                }else{
                    getMvpView().failure(response.message());
                }
            }

            @Override
            public void onFailure(Call<TermResponse> call, Throwable t) {
                getMvpView().failure(t.getLocalizedMessage());
            }
        });
    }
}
