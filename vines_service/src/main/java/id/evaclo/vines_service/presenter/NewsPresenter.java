package id.evaclo.vines_service.presenter;

import android.content.Context;

import id.evaclo.vines_service.BuildConfig;
import id.evaclo.vines_service.core.BasePresenter;
import id.evaclo.vines_service.model.news.NewsDetailRequest;
import id.evaclo.vines_service.model.news.NewsDetailResponse;
import id.evaclo.vines_service.model.news.NewsListRequest;
import id.evaclo.vines_service.model.news.NewsListResponse;
import id.evaclo.vines_service.service.VinesServices;
import id.evaclo.vines_service.service.VinesSession;
import id.evaclo.vines_service.view.NewsView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewsPresenter extends BasePresenter<NewsView> {
    Context context;
    VinesSession vinesSession;

    public NewsPresenter(Context context, VinesSession vinesSession) {
        this.context = context;
        this.vinesSession = vinesSession;
    }

    public void getListNews(){
        NewsListRequest request = new NewsListRequest(10,0);
        Call<NewsListResponse> call = VinesServices.createWithAuth(BuildConfig.BASE_URL,vinesSession).getListNews(request);
        call.enqueue(new Callback<NewsListResponse>() {
            @Override
            public void onResponse(Call<NewsListResponse> call, Response<NewsListResponse> response) {
                if(response.isSuccessful()){
                    if(response.body().getStatus() == 200){
                        getMvpView().onSuccessGetListNews(response.body());
                    }else {
                        getMvpView().failure(response.message());
                    }
                }else{
                    getMvpView().failure(response.message());
                }
            }

            @Override
            public void onFailure(Call<NewsListResponse> call, Throwable t) {
                getMvpView().failure(t.getLocalizedMessage());
            }
        });
    }
    public  void getDetailNews(int news_id){
        NewsDetailRequest request = new NewsDetailRequest(news_id);
        Call<NewsDetailResponse> call = VinesServices.createWithAuth(BuildConfig.BASE_URL,vinesSession).getDetailNews(request);
        call.enqueue(new Callback<NewsDetailResponse>() {
            @Override
            public void onResponse(Call<NewsDetailResponse> call, Response<NewsDetailResponse> response) {
                if(response.isSuccessful()){
                    if(response.body().getStatus() == 200){
                        getMvpView().onSuccessGetDetailtNews(response.body());
                    }else {
                        getMvpView().failure(response.message());
                    }
                }else{
                    getMvpView().failure(response.message());
                }
            }

            @Override
            public void onFailure(Call<NewsDetailResponse> call, Throwable t) {
                getMvpView().failure(t.getLocalizedMessage());
            }
        });
    }
}
