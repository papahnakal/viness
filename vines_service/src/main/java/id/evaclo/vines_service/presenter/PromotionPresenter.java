package id.evaclo.vines_service.presenter;

import android.content.Context;

import id.evaclo.vines_service.BuildConfig;
import id.evaclo.vines_service.core.BasePresenter;
import id.evaclo.vines_service.model.news.NewsListResponse;
import id.evaclo.vines_service.model.promotion.PromotionDetailRequest;
import id.evaclo.vines_service.model.promotion.PromotionListProductResponse;
import id.evaclo.vines_service.model.promotion.PromotionListRequest;
import id.evaclo.vines_service.model.promotion.PromotionListResponse;
import id.evaclo.vines_service.service.VinesServices;
import id.evaclo.vines_service.service.VinesSession;
import id.evaclo.vines_service.view.PromotionView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PromotionPresenter extends BasePresenter<PromotionView> {
    Context context;
    VinesSession vinesSession;

    public PromotionPresenter(Context context, VinesSession vinesSession) {
        this.context = context;
        this.vinesSession = vinesSession;
    }
    public void getListPromotion(int limit){
        PromotionListRequest request = new PromotionListRequest(limit);
        Call<PromotionListResponse> call = VinesServices.createWithAuth(BuildConfig.BASE_URL,vinesSession).getListPromotion(request);
        call.enqueue(new Callback<PromotionListResponse>() {
            @Override
            public void onResponse(Call<PromotionListResponse> call, Response<PromotionListResponse> response) {
                if(response.isSuccessful()){
                    if(response.body().getStatus() == 200){
                        getMvpView().onSuccessGetListPromotion(response.body());
                    }else {
                        getMvpView().failure(response.message());
                    }
                }else{
                    getMvpView().failure(response.message());
                }
            }

            @Override
            public void onFailure(Call<PromotionListResponse> call, Throwable t) {
                getMvpView().failure(t.getLocalizedMessage());
            }
        });
    }
    public void getDetailPromotion(int promotion_id, int limit, int offset){
        PromotionDetailRequest request = new PromotionDetailRequest(promotion_id,limit,offset);
        Call<PromotionListProductResponse> call = VinesServices.createWithAuth(BuildConfig.BASE_URL,vinesSession).getDetailPromotion(request);
        call.enqueue(new Callback<PromotionListProductResponse>() {
            @Override
            public void onResponse(Call<PromotionListProductResponse> call, Response<PromotionListProductResponse> response) {
                if(response.isSuccessful()){
                    if(response.body().getStatus() == 200){
                        getMvpView().onSuccessGetDetailPromotion(response.body());
                    }else {
                        getMvpView().failure(response.body().getDisplay_message());
                    }
                }else{
                    getMvpView().failure(response.message());
                }
            }

            @Override
            public void onFailure(Call<PromotionListProductResponse> call, Throwable t) {
                getMvpView().failure(t.getLocalizedMessage());
            }
        });
    }
}
