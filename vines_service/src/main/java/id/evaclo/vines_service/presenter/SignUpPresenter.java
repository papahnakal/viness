package id.evaclo.vines_service.presenter;

import android.content.Context;
import android.graphics.Bitmap;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import id.evaclo.vines_service.BuildConfig;
import id.evaclo.vines_service.core.BasePresenter;
import id.evaclo.vines_service.model.register.RegisterRequest;
import id.evaclo.vines_service.model.register.RegisterResponse;
import id.evaclo.vines_service.service.Header;
import id.evaclo.vines_service.service.VinesServices;
import id.evaclo.vines_service.service.VinesSession;
import id.evaclo.vines_service.view.SignUpView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpPresenter extends BasePresenter<SignUpView> {
    Context context;
    VinesSession vinesSession;

    public SignUpPresenter(Context context, VinesSession vinesSession) {
        this.context = context;
        this.vinesSession = vinesSession;
    }

    public List<Header> setHeaders(){
        Header header1 = new Header("Content-Type", "application/json");
        Header header2 = new Header("apikey", BuildConfig.APIKEY);
        List<Header> headers = new ArrayList();
        headers.add(header1);
        headers.add(header2);
        return headers;
    }

    public void postRegister(String name, String email, String password, String phone, String dob, Bitmap image){
        //RegisterRequest request = new RegisterRequest(name,email,password,phone,BuildConfig.TOKEN_ID,"-6.2732980","106.8694690");
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        Bitmap.createScaledBitmap(image,800,800,false);
        image.compress(Bitmap.CompressFormat.JPEG, 70, bos);

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date());

        RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), bos.toByteArray());
        //            File upload_Image = MultipartBody.Part.createFormData("upload_image",image_upload.getName(),requestFile);
        MultipartBody.Part uploadImageBody = MultipartBody.Part.createFormData(
                "image",
                "IMG_"+timeStamp,
                requestFile);
        RequestBody names = RequestBody.create(okhttp3.MediaType.parse("text/plain"), name);
        RequestBody emails = RequestBody.create(okhttp3.MediaType.parse("text/plain"), email);
        RequestBody passwords = RequestBody.create(okhttp3.MediaType.parse("text/plain"), password);
        RequestBody phones = RequestBody.create(okhttp3.MediaType.parse("text/plain"), phone);
        RequestBody token_id = RequestBody.create(okhttp3.MediaType.parse("text/plain"), BuildConfig.TOKEN_ID);
        RequestBody longitudes = RequestBody.create(okhttp3.MediaType.parse("text/plain"), "-6.2732980");
        RequestBody latitudes = RequestBody.create(okhttp3.MediaType.parse("text/plain"), "106.8694690");
        RequestBody dobs = RequestBody.create(okhttp3.MediaType.parse("text/plain"), dob);
        Call<RegisterResponse> call = VinesServices.createWithAuth(BuildConfig.BASE_URL,vinesSession)
                .postRegister(
                        names,
                        emails,
                        passwords,
                        phones,
                        token_id,
                        longitudes,
                        latitudes,
                        dobs,
                        uploadImageBody);
        call.enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                if(response.isSuccessful()){
                    RegisterResponse loginResponse = response.body();
                    if(loginResponse.getStatus()==200) {
                        getMvpView().onSuccess(response.body());
                        getMvpView().showMessage(response.message());
                    }else{
                        getMvpView().onFailed(response.body().getDisplay_message());
                        getMvpView().showMessage(response.body().getDisplay_message());
                    }
                }else{
                    getMvpView().onFailed(response.body().getMessage());
                    getMvpView().showMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                getMvpView().onFailed(t.getLocalizedMessage());
            }
        });

    }
}
