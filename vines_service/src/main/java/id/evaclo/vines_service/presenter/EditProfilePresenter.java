package id.evaclo.vines_service.presenter;

import android.content.Context;
import android.graphics.Bitmap;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import id.evaclo.vines_service.BuildConfig;
import id.evaclo.vines_service.core.BasePresenter;
import id.evaclo.vines_service.model.login.EditProfileResponse;
import id.evaclo.vines_service.model.login.UpdatePasswordRequest;
import id.evaclo.vines_service.model.login.UpdatePasswordResponse;
import id.evaclo.vines_service.service.VinesServices;
import id.evaclo.vines_service.service.VinesSession;
import id.evaclo.vines_service.view.EditProfileView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfilePresenter extends BasePresenter<EditProfileView> {
    Context context;
    VinesSession vinesSession;

    public EditProfilePresenter(Context context, VinesSession vinesSession) {
        this.context = context;
        this.vinesSession = vinesSession;
    }

    public void updateProfile(
            String user_id,
            String first_name,
            String last_name,
            String email,
            String phone,
            String address,
            String  dob,
            String token,
            Bitmap image
    ){
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        Bitmap.createScaledBitmap(image,800,800,false);
        image.compress(Bitmap.CompressFormat.JPEG, 70, bos);

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date());

        RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), bos.toByteArray());
        //            File upload_Image = MultipartBody.Part.createFormData("upload_image",image_upload.getName(),requestFile);
        MultipartBody.Part uploadImageBody = MultipartBody.Part.createFormData(
                "image",
                "IMG_"+timeStamp,
                requestFile);
        //RequestBody imageFile = RequestBody.create(okhttp3.MediaType.parse("image/jpeg"), image);

        RequestBody userId = RequestBody.create(okhttp3.MediaType.parse("text/plain"), user_id);
        RequestBody firsName = RequestBody.create(okhttp3.MediaType.parse("text/plain"), first_name);
        RequestBody lastName = RequestBody.create(okhttp3.MediaType.parse("text/plain"), last_name);
        RequestBody emaiL = RequestBody.create(okhttp3.MediaType.parse("text/plain"), email);
        RequestBody phonE = RequestBody.create(okhttp3.MediaType.parse("text/plain"), phone);
        RequestBody addresS = RequestBody.create(okhttp3.MediaType.parse("text/plain"), address);
        RequestBody doB = RequestBody.create(okhttp3.MediaType.parse("text/plain"), dob);
        RequestBody tokeN = RequestBody.create(okhttp3.MediaType.parse("text/plain"), token);
        Call<EditProfileResponse> call = VinesServices.createWithAuth(BuildConfig.BASE_URL,vinesSession)
                .postEditProfile(userId,firsName,lastName,emaiL,phonE,addresS,doB,tokeN,uploadImageBody);
        call.enqueue(new Callback<EditProfileResponse>() {
            @Override
            public void onResponse(Call<EditProfileResponse> call, Response<EditProfileResponse> response) {
                if(response.isSuccessful()){
                    if(response.body().getStatus() == 200){
                        getMvpView().onSuccessEditProfile(response.body());
                    }else {
                        getMvpView().onFailure(response.message());
                    }
                }else{
                    getMvpView().onFailure(response.message());
                }
            }

            @Override
            public void onFailure(Call<EditProfileResponse> call, Throwable t) {
                getMvpView().onFailure(t.getLocalizedMessage());
            }
        });
    }
}
