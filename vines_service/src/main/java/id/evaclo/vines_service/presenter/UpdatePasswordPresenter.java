package id.evaclo.vines_service.presenter;

import android.content.Context;

import id.evaclo.vines_service.BuildConfig;
import id.evaclo.vines_service.core.BasePresenter;
import id.evaclo.vines_service.model.contact.ContactResponse;
import id.evaclo.vines_service.model.login.UpdatePasswordRequest;
import id.evaclo.vines_service.model.login.UpdatePasswordResponse;
import id.evaclo.vines_service.service.VinesServices;
import id.evaclo.vines_service.service.VinesSession;
import id.evaclo.vines_service.view.UpdatePasswordView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdatePasswordPresenter extends BasePresenter<UpdatePasswordView> {
    Context context;
    VinesSession vinesSession;

    public UpdatePasswordPresenter(Context context, VinesSession vinesSession) {
        this.context = context;
        this.vinesSession = vinesSession;
    }

    public void updatePassword(String token, String new_password, String email){
        UpdatePasswordRequest request = new UpdatePasswordRequest(token,new_password,email);
        Call<UpdatePasswordResponse> call = VinesServices.createWithAuth(BuildConfig.BASE_URL,vinesSession).postChangePassword(request);
        call.enqueue(new Callback<UpdatePasswordResponse>() {
            @Override
            public void onResponse(Call<UpdatePasswordResponse> call, Response<UpdatePasswordResponse> response) {
                if(response.isSuccessful()){
                    if(response.body().getStatus() == 200){
                        getMvpView().onSuccessUpdatePassword(response.body());
                    }else {
                        getMvpView().onFailure(response.message());
                    }
                }else{
                    getMvpView().onFailure(response.message());
                }
            }

            @Override
            public void onFailure(Call<UpdatePasswordResponse> call, Throwable t) {
                getMvpView().onFailure(t.getLocalizedMessage());
            }
        });
    }
}
