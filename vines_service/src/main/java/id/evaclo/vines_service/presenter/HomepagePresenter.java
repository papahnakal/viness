package id.evaclo.vines_service.presenter;

import android.content.Context;

import id.evaclo.vines_service.BuildConfig;
import id.evaclo.vines_service.core.BasePresenter;
import id.evaclo.vines_service.model.promotion.PromotionListRequest;
import id.evaclo.vines_service.model.promotion.PromotionListResponse;
import id.evaclo.vines_service.model.store.StoreListRequest;
import id.evaclo.vines_service.model.store.StoreListResponse;
import id.evaclo.vines_service.service.VinesServices;
import id.evaclo.vines_service.service.VinesSession;
import id.evaclo.vines_service.view.HomepageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomepagePresenter extends BasePresenter<HomepageView> {
    Context context;
    VinesSession vinesSession;

    public HomepagePresenter(Context context, VinesSession vinesSession) {
        this.context = context;
        this.vinesSession = vinesSession;
    }

    public void getListPromotion(int limit){
        PromotionListRequest request = new PromotionListRequest(limit);
        Call<PromotionListResponse> call = VinesServices.createWithAuth(BuildConfig.BASE_URL,vinesSession).getListPromotion(request);
        call.enqueue(new Callback<PromotionListResponse>() {
            @Override
            public void onResponse(Call<PromotionListResponse> call, Response<PromotionListResponse> response) {
                if(response.isSuccessful()){
                    if(response.body().getStatus() == 200){
                        getMvpView().onSuccessGetListPromotion(response.body());
                    }else {
                        getMvpView().failure(response.message());
                    }
                }else{
                    getMvpView().failure(response.message());
                }
            }

            @Override
            public void onFailure(Call<PromotionListResponse> call, Throwable t) {
                getMvpView().failure(t.getLocalizedMessage());
            }
        });
    }
    public void getListStore(Double lng, Double lat){
        StoreListRequest request = new StoreListRequest(lat,lng);
        Call<StoreListResponse>call = VinesServices.createWithAuth(BuildConfig.BASE_URL,vinesSession).getListStore(request);
        call.enqueue(new Callback<StoreListResponse>() {
            @Override
            public void onResponse(Call<StoreListResponse> call, Response<StoreListResponse> response) {
                if(response.isSuccessful()){
                    if(response.body().getStatus() == 200){
                        getMvpView().onSuccessGetListStore(response.body());
                    }else {
                        getMvpView().failure(response.message());
                    }
                }else{
                    getMvpView().failure(response.message());
                }
            }

            @Override
            public void onFailure(Call<StoreListResponse> call, Throwable t) {
                getMvpView().failure(t.getLocalizedMessage());
            }
        });
    }
}
