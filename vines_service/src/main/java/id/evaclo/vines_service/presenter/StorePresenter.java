package id.evaclo.vines_service.presenter;

import android.content.Context;

import id.evaclo.vines_service.BuildConfig;
import id.evaclo.vines_service.core.BasePresenter;
import id.evaclo.vines_service.model.register.RegisterResponse;
import id.evaclo.vines_service.model.store.StoreDetailRequest;
import id.evaclo.vines_service.model.store.StoreDetailResponse;
import id.evaclo.vines_service.model.store.StoreListRequest;
import id.evaclo.vines_service.model.store.StoreListResponse;
import id.evaclo.vines_service.service.VinesServices;
import id.evaclo.vines_service.service.VinesSession;
import id.evaclo.vines_service.view.StoreView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StorePresenter extends BasePresenter<StoreView> {
    Context context;
    VinesSession vinesSession;

    public StorePresenter(Context context, VinesSession vinesSession) {
        this.context = context;
        this.vinesSession = vinesSession;
    }

    public void getListStore(Double lng, Double lat){
        StoreListRequest request = new StoreListRequest(lat,lng);
        Call<StoreListResponse>call = VinesServices.createWithAuth(BuildConfig.BASE_URL,vinesSession).getListStore(request);
        call.enqueue(new Callback<StoreListResponse>() {
            @Override
            public void onResponse(Call<StoreListResponse> call, Response<StoreListResponse> response) {
                if(response.isSuccessful()){
                    if(response.body().getStatus() == 200){
                        getMvpView().onSuccessGetListStore(response.body());
                    }else {
                        getMvpView().failure(response.message());
                    }
                }else{
                    getMvpView().failure(response.message());
                }
            }

            @Override
            public void onFailure(Call<StoreListResponse> call, Throwable t) {
                getMvpView().failure(t.getLocalizedMessage());
            }
        });
    }

    public void getDetailStore(int idStore){
        StoreDetailRequest request = new StoreDetailRequest(idStore);
        Call<StoreDetailResponse>call = VinesServices.createWithAuth(BuildConfig.BASE_URL,vinesSession).getDetailStore(request);
        call.enqueue(new Callback<StoreDetailResponse>() {
            @Override
            public void onResponse(Call<StoreDetailResponse> call, Response<StoreDetailResponse> response) {
                if(response.isSuccessful()){
                    if(response.body().getStatus() == 200){
                        getMvpView().onSuccessGetDetailStore(response.body());
                    }else {
                        getMvpView().failure(response.message());
                    }
                }else{
                    getMvpView().failure(response.message());
                }
            }

            @Override
            public void onFailure(Call<StoreDetailResponse> call, Throwable t) {
                getMvpView().failure(t.getLocalizedMessage());
            }
        });
    }


}
