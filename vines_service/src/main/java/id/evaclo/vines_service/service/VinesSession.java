package id.evaclo.vines_service.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import id.evaclo.vines_service.model.login.LoginResponse;
import id.evaclo.vines_service.model.product.ProductDetailResponse;

public class VinesSession implements VinesSessionManager {
    private SharedPreferences mPref;
    private SharedPreferences.Editor mEditor;
    private Context mContext;

    private static int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "vines";
    private static final String KEY_USER_DATA = "user";
    private static final String KEY_USER_TOKEN = "token";
    private static final String KEY_CART = "cart";

    public VinesSession(Context context) {
        this.mContext = context;
        this.mPref = mContext.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        this.mEditor = mPref.edit();
    }

    public static VinesSession create(Context context) {
        return new VinesSession(context);
    }

    @Override
    public void clear() {
        mEditor.clear();
        mEditor.commit();
    }

    @Override
    public void clearCart() {
        mEditor.remove(KEY_CART);
        mEditor.apply();
    }

    @Override
    public String getToken() {
        return mPref.getString(KEY_USER_TOKEN,"");
    }

    @Override
    public void storeResponse(LoginResponse response) {
        mEditor.putString(KEY_USER_TOKEN,response.getData().getToken());
        Gson gson = new Gson();
        String dataUser = gson.toJson(response);
        Log.d("json set data user",dataUser);
        mEditor.putString(KEY_USER_DATA,dataUser);
        mEditor.apply();
    }

    @Override
    public LoginResponse getLoginResponse() {
        Gson gson = new Gson();
        String json = mPref.getString(KEY_USER_DATA,"");
        Log.d("json get acount",json);
        LoginResponse user = gson.fromJson(json, LoginResponse.class);
        return user;
    }

    @Override
    public void setCart(List<ProductDetailResponse.Data> data) {
        Gson gson = new Gson();
        String dataCart = gson.toJson(data);
        Log.d("json set data cart",dataCart);
        mEditor.putString(KEY_CART,dataCart);
        mEditor.apply();
    }

    @Override
    public List<ProductDetailResponse.Data> getCart() {
        Gson gson = new Gson();
        List<ProductDetailResponse.Data> carts;
        String json = mPref.getString(KEY_CART,"");
        Type type = new TypeToken<List<ProductDetailResponse.Data>>() {
        }.getType();
        Log.d("json get car",json);
        carts= gson.fromJson(json, type);
        return carts;
    }


}
