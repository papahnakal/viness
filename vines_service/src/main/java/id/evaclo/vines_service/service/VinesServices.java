package id.evaclo.vines_service.service;

import android.os.Build;

import java.util.ArrayList;
import java.util.List;

import id.evaclo.vines_service.BuildConfig;

public class VinesServices {

    public static VinesAPI create(String baseurl) {
        List<Header> header = new ArrayList<>();
        Header header1 = new Header("apikey",BuildConfig.APIKEY);
        Header header2 = new Header("Content-Type","application/json");
        header.add(header1);
        header.add(header2);
        return Generator.create(header, baseurl , VinesAPI.class);
    }

    public static VinesAPI createWithAuth(String baseurl,VinesSession vinesSession) {
        List<Header> header = new ArrayList<>();
        Header header1 = new Header("apikey",BuildConfig.APIKEY);
        Header header2 = new Header("Content-Type","application/json");
        header.add(header1);
        header.add(header2);
        return Generator.create(header, baseurl, VinesAPI.class);
    }

}
