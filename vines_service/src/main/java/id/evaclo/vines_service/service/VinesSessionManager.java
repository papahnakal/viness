package id.evaclo.vines_service.service;

import java.util.List;

import id.evaclo.vines_service.model.login.LoginResponse;
import id.evaclo.vines_service.model.product.ProductDetailResponse;

public interface VinesSessionManager {
    void clear();

    void clearCart();

    String getToken();

    void storeResponse(LoginResponse data);

    LoginResponse getLoginResponse();

    void setCart(List<ProductDetailResponse.Data> data);

    List<ProductDetailResponse.Data> getCart();

}
