package id.evaclo.vines_service.service;

import id.evaclo.vines_service.model.order.GetOrderCodeRequest;
import id.evaclo.vines_service.model.PartyRequest;
import id.evaclo.vines_service.model.PartyResponse;
import id.evaclo.vines_service.model.TermResponse;
import id.evaclo.vines_service.model.about.AboutResponse;
import id.evaclo.vines_service.model.contact.ContactRequest;
import id.evaclo.vines_service.model.contact.ContactResponse;
import id.evaclo.vines_service.model.login.EditProfileResponse;
import id.evaclo.vines_service.model.login.UpdatePasswordRequest;
import id.evaclo.vines_service.model.login.UpdatePasswordResponse;
import id.evaclo.vines_service.model.order.GetOrderCodeResponse;
import id.evaclo.vines_service.model.order.HistoryOrderRequest;
import id.evaclo.vines_service.model.order.HistoryOrderResponse;
import id.evaclo.vines_service.model.login.AuthenticateRequest;
import id.evaclo.vines_service.model.login.ForgotPasswordRequest;
import id.evaclo.vines_service.model.login.ForgotPasswordResponse;
import id.evaclo.vines_service.model.login.LoginResponse;
import id.evaclo.vines_service.model.news.NewsDetailRequest;
import id.evaclo.vines_service.model.news.NewsDetailResponse;
import id.evaclo.vines_service.model.news.NewsListRequest;
import id.evaclo.vines_service.model.news.NewsListResponse;
import id.evaclo.vines_service.model.order.RecentOrderRequest;
import id.evaclo.vines_service.model.order.RecentOrderResponse;
import id.evaclo.vines_service.model.product.ProductDetailRequest;
import id.evaclo.vines_service.model.product.ProductDetailResponse;
import id.evaclo.vines_service.model.product.ProductFavoriteListResponse;
import id.evaclo.vines_service.model.product.ProductFavoriteRequest;
import id.evaclo.vines_service.model.product.ProductListRequest;
import id.evaclo.vines_service.model.product.ProductListResponse;
import id.evaclo.vines_service.model.product.ProductSimilarListResponse;
import id.evaclo.vines_service.model.product.ProductSimilarRequest;
import id.evaclo.vines_service.model.promotion.PromotionDetailRequest;
import id.evaclo.vines_service.model.promotion.PromotionListProductResponse;
import id.evaclo.vines_service.model.promotion.PromotionListRequest;
import id.evaclo.vines_service.model.promotion.PromotionListResponse;
import id.evaclo.vines_service.model.register.RegisterResponse;
import id.evaclo.vines_service.model.store.StoreDetailRequest;
import id.evaclo.vines_service.model.store.StoreDetailResponse;
import id.evaclo.vines_service.model.store.StoreListRequest;
import id.evaclo.vines_service.model.store.StoreListResponse;
import id.evaclo.vines_service.model.wishlist.AddWishListRequest;
import id.evaclo.vines_service.model.wishlist.AddWishListResponse;
import id.evaclo.vines_service.model.wishlist.WishListRequest;
import id.evaclo.vines_service.model.wishlist.WishListResponse;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface VinesAPI {

    @POST("user/authentication")
    Call<LoginResponse> getAuthentication(
            @Body AuthenticateRequest request
    );
    @Multipart
    @POST("user/register")
    Call<RegisterResponse> postRegister(
            @Part("fullname") RequestBody fullname,
            @Part("email") RequestBody email,
            @Part("password")RequestBody password,
            @Part("phone")RequestBody phone,
            @Part("token_id")RequestBody token_id,
            @Part("longitude")RequestBody longitude,
            @Part("latitude")RequestBody latitude,
            @Part("dob")RequestBody dob,
            @Part MultipartBody.Part image
    );
    @POST("user/update_password")
    Call<ForgotPasswordResponse>postForgotPassword(
            @Body ForgotPasswordRequest request
    );
    @POST("store/list")
    Call<StoreListResponse>getListStore(
            @Body StoreListRequest request
    );
    @POST("store/detail")
    Call<StoreDetailResponse>getDetailStore(
            @Body StoreDetailRequest request
    );
    @POST("product/list")
    Call<ProductListResponse>getProductList(
            @Body ProductListRequest request
    );
    @POST("product/similiar")
    Call<ProductSimilarListResponse>getProductSimilar(
            @Body ProductSimilarRequest request
    );
    @POST("product/favourite")
    Call<ProductFavoriteListResponse>getProductFavorite(
            @Body ProductFavoriteRequest request
    );
    @POST("product/detail")
    Call<ProductDetailResponse>getProductDetail(
            @Body ProductDetailRequest request
    );
    @POST("user/wishlist")
    Call<WishListResponse>getListWishList(
            @Body WishListRequest request
    );
    @POST("user/add_wishlist")
    Call<AddWishListResponse>addWishList(
            @Body AddWishListRequest request
    );
    @POST("user/delete_wishlist")
    Call<AddWishListResponse>deleteWishList(
            @Body AddWishListRequest request
    );
    @POST("news")
    Call<NewsListResponse>getListNews(
            @Body NewsListRequest request
    );
    @POST("news/detail")
    Call<NewsDetailResponse>getDetailNews(
            @Body NewsDetailRequest request
    );
    @POST("user/history_order")
    Call<HistoryOrderResponse>getHistoryOrder(
            @Body HistoryOrderRequest request
    );
    @POST("user/current_order")
    Call<RecentOrderResponse>getRecentOrder(
            @Body RecentOrderRequest request
    );
    @POST("promotions")
    Call<PromotionListResponse>getListPromotion(
            @Body PromotionListRequest request
    );
    @POST("promotions/product")
    Call<PromotionListProductResponse>getDetailPromotion(
            @Body PromotionDetailRequest response
    );
    @POST("about")
    Call<AboutResponse> getAbout();
    @POST("contact")
    Call<ContactResponse>postContact(
            @Body ContactRequest request
    );

    @POST("party_service")
    Call<PartyResponse>postParty(
            @Body PartyRequest request
    );
    @POST("user/update_password")
    Call<UpdatePasswordResponse>postChangePassword(
            @Body UpdatePasswordRequest request
            );
    @POST("term_conditions")
    Call<TermResponse>postGetTermAndConfition();
    @POST("privacy_policy")
    Call<TermResponse>postGetPrivacy();

    @Multipart
    @POST("user/edit_profile")
    Call<EditProfileResponse> postEditProfile(
            @Part("user_id") RequestBody user_id,
            @Part("first_name") RequestBody first_name,
            @Part("last_name")RequestBody last_name,
            @Part("email")RequestBody email,
            @Part("phone")RequestBody phone,
            @Part("address")RequestBody address,
            @Part("dob")RequestBody dob,
            @Part("token")RequestBody token,
            @Part MultipartBody.Part image
            //@Part MultipartBody.Part image
    );
    @POST("/get_ordercode")
    Call<GetOrderCodeResponse> getOrderCode(
            @Body GetOrderCodeRequest request
    );
}
